export interface User {
  _id: string;
  userId: string;
  userEmail?: string;
  mobile?: number | string;
  firstName: string;
  lastName: string;
  emailVerified: boolean;
  mobileVerified: boolean;
  username: string;
  registeredAt: string;
  sessionId: string;
}

export interface Product {
  mainImg: string | StaticImageData;
  media: string[] | StaticImageData[];
  name: string;
  type: 'veg' | 'nonveg';
  avgRating: number;
  totalRatings: number;
  ratings: {
    5: number;
    4: number;
    3: number;
    2: number;
    1: number;
  };
  mrp: number;
  price: number;
  tags: string[];
  details: {
    serving: number;
    description: string;
    inKit: string[];
    fromPantry: string[];
    cookingTime: string; // eg -> 10 - 15 mins
    allergens: string[];
  };
  reviews: Review[];
}

export interface Review {
  user: string;
  rating: number;
  title: string;
  description: string;
  media?: string[] | StaticImageData[];
  reviewedAt: string;
}

export interface Address {
  type: string;
  address: string;
  area: string;
  city: string;
  pincode: string;
  landmark?: string;
}

export interface CheckoutDetails {
  products: {
    product: Product;
    qty: number;
  }[];
  subtotal: number;
  shipping: number;
  discount: number;
  total: number;
  coupons?: string[];
}

export interface Order {
  no: string;
  status: 'Order Placed' | 'Shipped' | 'On the Way' | 'Delivered';
  orderedOn: string;
  deliveryScheduled: string;
  timeSlot: 'lunch' | 'dinner';
  paymentStatus: 'Pending' | 'Completed';
  paymentMode: string;
  address: Address;
  productDetails: CheckoutDetails;
  specialInstruction?: string;
}
