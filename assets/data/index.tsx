// interface
import { Product, Order, Address, Review } from '../../interface/index';

// data
import pImg from '../images/pImage.png';

export const reviews: Review[] = [
  {
    user: 'Yash Gupta',
    rating: 3,
    title: 'Great Product Totally Loved it!',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum efficitur finibus. Proin ex orci, bibendum a tristique sed, tincidunt sit amet nisi. Duis laoreet tempus pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum efficitur finibus. Proin ex orci, bibendum a tristique sed, tincidunt sit amet nisi. Duis laoreet tempus pretium. ',
    reviewedAt: '12/02/2022 12:00:05',
  },
  {
    user: 'Shivam Jindal',
    rating: 5,
    title: 'Great Product Totally Loved it!',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum efficitur finibus. Proin ex orci, bibendum a tristique sed, tincidunt sit amet nisi. Duis laoreet tempus pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vestibulum efficitur finibus. Proin ex orci, bibendum a tristique sed, tincidunt sit amet nisi. Duis laoreet tempus pretium. ',
    reviewedAt: '12/02/2022 10:00:05',
  },
];

export const products: Product[] = [
  {
    name: 'Paneer Butter Masala',
    mainImg: pImg,
    media: [],
    type: 'veg',
    avgRating: 4,
    totalRatings: 32,
    ratings: {
      5: 16,
      4: 10,
      3: 5,
      2: 1,
      1: 0,
    },
    mrp: 349,
    price: 199,
    tags: ['veg', 'craving'],
    details: {
      serving: 2,
      description:
        'Remember that delicious aroma from the kitchen and your mom would call you for lunch and the taste felt so irresistible, that it filled you with energy and satisfaction? Experience home away from home with kitchen kindle’s Paneer Butter Masala, a taste of satisfaction, packed full of protein and cooked in creamy makhani gravy within minutes that will keep the blues away while bringing that nostalgia back.',
      inKit: [
        'Paneer',
        'Tomato Paste',
        'Onion mix',
        'Cream Fresh',
        'Butter',
        'Paneer Butter Seasoning',
      ],
      fromPantry: ['Water'],
      cookingTime: '10 - 15 mins',
      allergens: ['Dairy', 'Gluten'],
    },
    reviews: reviews,
  },
];
