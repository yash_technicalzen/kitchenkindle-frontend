import React from 'react';
// components
import {
  Breadcrumbs,
  Layout,
  ProductDetail,
  ProductOverview,
  ProductReview,
  YouMayAlsoLike,
} from '../../../components';
// material ui components
import { Tabs, Tab } from '@mui/material';

const ProductDetailsPage = () => {
  // state
  const [value, setValue] = React.useState('1');

  // function
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };
  return (
    <Layout pageTitle="Product" header={true} footer={true}>
      <>
        <Breadcrumbs />
        <ProductOverview />
        <div className="mb-10 border-b-2">
          <Tabs value={value} onChange={handleChange}>
            <Tab value="1" label="Product Details" />
            <Tab value="2" label="Reviews" />
          </Tabs>
        </div>
        <div className="mb-10">
          {value === '1' ? <ProductDetail /> : <ProductReview />}
        </div>
        <YouMayAlsoLike />
      </>
    </Layout>
  );
};

export default ProductDetailsPage;
