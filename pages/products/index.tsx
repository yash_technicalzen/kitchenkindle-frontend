import { useRouter } from 'next/router';
import React from 'react';
// components
import {
  AllFilteredProducts,
  Layout,
  PageBanner,
  ProductFilter,
} from '../../components';

const AllProducts = () => {
  const router = useRouter();
  const title = 'All Products';
  const breadcrumbs = [
    {
      title: 'Products',
      route: '/products',
    },
  ];

  return (
    <Layout pageTitle="Products" header={true} footer={true}>
      <>
        <PageBanner title={title} breadcrumbs={breadcrumbs} />
        <div className="w-100 flex">
          <div className="w-[25%]">
            <ProductFilter />
          </div>
          <div className="w-[75%]">
            <AllFilteredProducts />
          </div>
        </div>
      </>
    </Layout>
  );
};

export default AllProducts;
