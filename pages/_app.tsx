import { useEffect } from 'react';
import '../styles/globals.css';
import { Provider } from 'react-redux';
import type { AppProps } from 'next/app';

// toast
import { ToastContainer } from 'react-toastify';
import { ToastMessage } from '../components';
import 'react-toastify/dist/ReactToastify.css';

// store
import { store } from '../redux/store';
// hooks
import { useAppSelector, useAppDispatch } from '../redux/hooks';
// reducer
import { checkLoggedInUser } from '../redux/reducers/auth.reducer';

function MyApp({ Component, pageProps }: AppProps) {
  const dispatch = useAppDispatch();
  // state
  const { infoMessage, successMessage, errorMessage } = useAppSelector(
    (state) => state.alertReducer
  );

  // functions
  useEffect(() => {
    dispatch(checkLoggedInUser());
  }, [dispatch]);

  useEffect(() => {
    if (infoMessage)
      ToastMessage({ type: 'info', message: infoMessage.message });
  }, [infoMessage]);

  useEffect(() => {
    if (successMessage)
      ToastMessage({ type: 'success', message: successMessage.message });
  }, [successMessage]);

  useEffect(() => {
    if (errorMessage)
      ToastMessage({ type: 'error', message: errorMessage.message });
  }, [errorMessage]);
  return (
    <>
      <Component {...pageProps} />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        draggable={false}
        theme={'colored'}
        closeOnClick
        pauseOnHover
      />
    </>
  );
}

function AppWrapper({ Component, pageProps, router }: AppProps) {
  return (
    <Provider store={store}>
      <MyApp pageProps={pageProps} Component={Component} router={router} />
    </Provider>
  );
}



export default AppWrapper;
