import { NextApiRequest, NextApiResponse } from 'next';
import { parseCookie } from '../../utils/cookie';
import axios from 'axios';

async function helperApi(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    try {
      const { url, method, options } = req.body;
      if (options.auth) {
        const { token } = parseCookie(req);
        if (!token)
          return res
            .status(401)
            .json({ success: false, data: null, error: 'Unauthorized' });

        options.headers = { ...options.headers, authorization: token };
      }
      const apiRes = await axios({
        method: method,
        url: `${process.env.NEXT_PUBLIC_API_ENDPOINT}${url}`,
        headers: { ...options.headers },
        data: { ...options?.payload },
      });

      return res.status(apiRes.status).json(apiRes.data);
    } catch (err: any) {
      if (err.response?.data) {
        res.status(err.response.status).json(err.response.data);
        return;
      }
      res.status(400).json({ success: false, data: null, error: err.message });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).json({
      success: false,
      data: null,
      error: `Method ${req.method} not allowed`,
    });
  }
}

export default helperApi;
