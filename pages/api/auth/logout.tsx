import { NextApiRequest, NextApiResponse } from 'next';
import cookie from 'cookie';

async function handleUser(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'GET') {
    try {
      // set cookie
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('token', '', {
          httpOnly: true,
          secure: process.env.NEXT_PUBLIC_VERCEL_ENV !== 'development',
          maxAge: 0,
          sameSite: 'strict',
          path: '/',
        })
      );

      return res
        .status(200)
        .json({ success: true, data: 'Logout Successfully', error: null });
    } catch (err: any) {
      if (err.response?.data) {
        res.status(err.response.status).json(err.response.data);
        return;
      }
      res.status(400).json({ success: true, data: null, error: err.message });
    }
  } else {
    res.setHeader('Allow', ['GET']);
    res.status(405).json({
      success: false,
      data: null,
      error: `Method ${req.method} not allowed`,
    });
  }
}

export default handleUser;
