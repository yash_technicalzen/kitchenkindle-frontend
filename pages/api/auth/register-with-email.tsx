import { NextApiRequest, NextApiResponse } from 'next';
import axios from 'axios';
import cookie from 'cookie';

async function handleRegister(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    try {
      const apiRes = await axios({
        method: 'POST',
        url: process.env.NEXT_PUBLIC_API_ENDPOINT + 'auth/register-with-email',
        headers: {},
        data: req.body,
      });

      const user = apiRes.data.data;
      const token = user.sessionId;
      delete user.sessionId;

      // set cookie
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('token', token, {
          httpOnly: true,
          secure: process.env.NEXT_PUBLIC_VERCEL_ENV !== 'development',
          maxAge: 60 * 60 * 24 * 7, // a week
          sameSite: 'strict',
          path: '/',
        })
      );

      res.status(200).json({ success: true, data: user, error: null });
    } catch (err: any) {
      if (err.response.data) {
        res.status(err.response.status).json(err.response.data);
        return;
      }
      res.status(400).json({ success: false, data: null, error: err.message });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).json({
      success: false,
      data: null,
      error: `Method ${req.method} not allowed`,
    });
  }
}

export default handleRegister;
