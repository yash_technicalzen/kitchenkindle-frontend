import React from 'react';
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from 'next';
import { Layout, LoginForm } from '../../components';
// protected route
import { isLoggedOut } from '../../hoc/protectedRoute';
// data
import form_image from '../../assets/images/form_image.png';
import Image from 'next/image';

const Login: NextPage = () => {
  return (
    <Layout pageTitle="Login">
      <div className="m-auto my-12 grid grid-cols-8 gap-0 min-h-fit max-w-[110rem] border border-gray-400 rounded-3xl">
        <div className="col-span-5 relative w-full h-full rounded-t-3xl rounded-l-3xl">
          <Image
            src={form_image}
            alt="Form"
            layout="fill"
            objectFit="fill"
            priority={true}
          />
        </div>
        <div className="col-span-3">
          <LoginForm />
        </div>
      </div>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = isLoggedOut(
  async (ctx: GetServerSidePropsContext) => {

    return {
      props: {},
    };
  }
);

export default Login;
