import { useRef } from 'react';
import type {
  NextPage,
  GetServerSideProps,
  GetServerSidePropsContext,
} from 'next';
import Slider from 'react-slick';

// hooks
import { useAppSelector } from '../redux/hooks';

// components
import {
  Layout,
  ImageSlider,
  DealsOfTheDay,
  ChoosePerYourMood,
  FeaturedProducts,
} from '../components';

// styles
import Classes from '../styles/Home.module.css';

// material ui icons
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

// data
import banner1 from '../assets/images/banner1.png';
import ban1 from '../assets/images/ban1.png';
import ban2 from '../assets/images/ban2.png';
import ban3 from '../assets/images/ban3.png';

const Home: NextPage = () => {
  // states
  const imageSliderImages: Array<StaticImageData> = [banner1, banner1];
  const bannerImages: Array<StaticImageData> = [
    ban1,
    ban2,
    ban3,
    ban1,
    ban2,
    ban3,
  ];
  const imageSliderSettings = {
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    arrows: false,
    dotsClass: 'sliderDots',
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 8000,
  };
  const bannerSettings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    adaptiveHeight: true,
    arrows: false,
    dotsClass: 'sliderDots',
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 8000,
  };

  // ref
  const sliderRef = useRef<Slider | null>(null);

  // functions
  const onRightClick = async () => {
    sliderRef?.current?.slickNext();
  };

  const onLeftClick = () => {
    sliderRef?.current?.slickPrev();
  };
  return (
    <Layout pageTitle="Kitchen Kindle" header={true} footer={true}>
      <>
        <div className="my-7">
          <ImageSlider
            images={imageSliderImages}
            settings={imageSliderSettings}
          />
        </div>
        <div className="my-7 d-flex relative">
          <div className={Classes.leftArrow}>
            <ArrowLeftIcon fontSize="large" onClick={onLeftClick} />
          </div>
          <ImageSlider
            images={bannerImages}
            settings={bannerSettings}
            sliderRef={sliderRef}
          />
          <div className={Classes.rightArrow} onClick={onRightClick}>
            <ArrowRightIcon fontSize="large" />
          </div>
        </div>
        <div className="my-24">
          <DealsOfTheDay />
        </div>
        <div className="my-24">
          <ChoosePerYourMood />
        </div>
        <div className="my-24">
          <FeaturedProducts />
        </div>
      </>
    </Layout>
  );
};

// for rendering if logged in
// export const getServerSideProps: GetServerSideProps = isLoggedIn(
//   async (ctx: GetServerSidePropsContext) => {

//     return {
//       props: {},
//     };
//   }
// );

// for rendering if logged out
// export const getServerSideProps: GetServerSideProps = isLoggedOut(
//   async (ctx: GetServerSidePropsContext) => {

//     return {
//       props: {},
//     };
//   }
// );

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext
) => {
  return {
    props: {},
  };
};

export default Home;
