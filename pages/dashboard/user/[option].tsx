import React, { useEffect } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { useRouter } from 'next/router';
import { DashboardWrapper, Layout } from '../../../components';
// protected route
import { isLoggedIn } from '../../../hoc/protectedRoute';

const UserDashboard = () => {
  const router = useRouter();
  // state
  const { option } = router.query;
  // function

  return (
    <Layout pageTitle="User Dashboard">
      <div className="w-full my-20 flex">
        <DashboardWrapper selectedOption={option?.toString()} />
      </div>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = isLoggedIn(
  async (ctx: GetServerSidePropsContext) => {
    return {
      props: {},
    };
  }
);

export default UserDashboard;
