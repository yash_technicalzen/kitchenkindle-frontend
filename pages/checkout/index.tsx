import React from 'react';
// components
import {
  Breadcrumbs,
  Layout,
  Checkout,
  OrderProductDetails,
} from '../../components';
// hooks
import { useAppSelector } from '../../redux/hooks';

const CheckoutPage = () => {
  // state
  const { products } = useAppSelector((state) => state.cart);
  return (
    <Layout pageTitle="Checkout" header={true} footer={true}>
      <>
        <Breadcrumbs />
        <div className="grid grid-cols-6 gap-10">
          <div className="col-span-4">
            <Checkout />
          </div>
          <div className="col-span-2 mt-10 mb-16">
            <OrderProductDetails products={[...products, ...products]} />
          </div>
        </div>
      </>
    </Layout>
  );
};

export default CheckoutPage;
