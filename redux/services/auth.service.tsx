// utils
import { get, success, error, post, apiCall } from './services.common';

// types
type LoginWithEmailProps = {
  email: string;
  password: string;
};

type RegisterWithEmailProps = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};

// methods
export async function loginWithEmail(payload: LoginWithEmailProps) {
  const url = `/api/auth/login-with-email`;
  const res: any = await post(url, {
    auth: false,
    payload: payload,
  });
  if (res.isSuccessful) {
    return success(res.data);
  } else {
    return error(res.error);
  }
}

export async function registerWithEmail(payload: RegisterWithEmailProps) {
  const url = `/api/auth/register-with-email`;
  const res: any = await post(url, {
    auth: false,
    payload: payload,
  });
  if (res.isSuccessful) {
    return success(res.data);
  } else {
    return error(res.error);
  }
}

export async function checkLoggedIn() {
  const url = 'user/';
  const res: any = await apiCall('GET', url, {
    auth: true,
  });

  if (res.isSuccessful) {
    return success(res.data);
  } else {
    return error(res.error);
  }
}

export async function logout() {
  const url = `/api/auth/logout`;
  const res: any = await get(url, {
    auth: false,
  });

  if (res.isSuccessful) {
    return success(res.data);
  } else {
    return error(res.error);
  }
}
