// utils
import { get, success, error, post, apiCall } from './services.common';

// interface
import { Product } from '../../interface';

// types
type AddToCartProps = {
  product: Product;
  qty: number;
};

// methods
export async function addToCart(payload: AddToCartProps) {
  return success(payload);
}

export async function subFromCart(payload: AddToCartProps) {
  return success(payload);
}

export async function delFromCart(payload: Product) {
  return success(payload);
}
