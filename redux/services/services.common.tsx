import axios, { AxiosRequestHeaders } from 'axios';

// api endpoint
export const api = process.env.NEXT_PUBLIC_API_ENDPOINT;

// service response
export const error = (error: string) => {
  return {
    status: 'error',
    isSuccessful: false,
    data: undefined,
    error: error,
  };
};
export const success = (data: any) => {
  return {
    status: 'success',
    isSuccessful: true,
    data: data,
    error: undefined,
  };
};

// type
type options = {
  headers?: AxiosRequestHeaders;
  auth: boolean;
  payload?: object;
};

// request methods
export const get = async (url: string, options: options) => {
  try {
    let headers: any = {};
    // if headers are passed
    if (options.headers) {
      headers = { ...headers, ...options.headers };
    }

    const response = await axios.get(url, { ...headers });

    if (response.data.success === true) {
      return success(response.data.data);
    } else {
      return error(response.data.error);
    }
  } catch (e: any) {
    if (e.response.data) {
      return error(e.response.data.error);
    }
    return error(e);
  }
};

export const post = async (url: string, options: options) => {
  try {
    let headers: any = {};
    // if headers are passed
    if (options.headers) {
      headers = { ...headers, ...options.headers };
    }

    const response = await axios.post(
      url,
      { ...options.payload },
      { ...headers }
    );

    if (response.data.success === true) {
      return success(response.data.data);
    } else {
      return error(response.data.error);
    }
  } catch (e: any) {
    if (e.response.data) {
      return error(e.response.data.error);
    }
    return error(e.message);
  }
};

export const del = async (url: string, options: options) => {
  try {
    let headers: any = {};
    // if headers are passed
    if (options.headers) {
      headers = { ...headers, ...options.headers };
    }

    const response = await axios.delete(url, { ...headers });

    if (response.data.success === true) {
      return success(response.data.data);
    } else {
      return error(response.data.error);
    }
  } catch (e: any) {
    if (e.response.data) {
      return error(e.response.data.error);
    }
    return error(e);
  }
};

export const apiCall = async (
  method: string,
  url: string,
  options: options
) => {
  try {
    const response = await axios.post('/api/helper', { url, method, options });
    if (response.data.success === true) {
      return success(response.data.data);
    } else {
      return error(response.data.error);
    }
  } catch (e: any) {
    if (e.response.data) {
      return error(e.response.data.error);
    }
    return error(e);
  }
};
