import { configureStore } from '@reduxjs/toolkit';

// reducers

// alert reducer
import alertReducer from './reducers/alert.reducer';

// auth reduces
import authReducer from './reducers/auth.reducer';

// cart reducer
import cartReducer from './reducers/cart.reducer';

export const store = configureStore({
  reducer: {
    alertReducer,
    authReducer,
    cart: cartReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
