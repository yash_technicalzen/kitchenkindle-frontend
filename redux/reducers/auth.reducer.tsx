import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// services
import {
  loginWithEmail,
  registerWithEmail,
  checkLoggedIn,
  logout,
} from '../services/auth.service';

// type
interface User {
  _id: string;
  userId: string;
  userEmail?: string;
  mobile?: number | string;
  firstName: string;
  lastName: string;
  emailVerified: boolean;
  mobileVerified: boolean;
  username: string;
  registeredAt: string;
  sessionId: string;
}

interface authState {
  loggedIn: boolean;
  user: User | undefined | null;
}

const initialState: authState = {
  loggedIn: false,
  user: null,
};

// extra reducers
export const loginUserWithEmail = createAsyncThunk(
  'auth/loginWithEmail',
  async (payload: any, thunkApi) => {
    const res = await loginWithEmail(payload);
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

export const registerUserWithEmail = createAsyncThunk(
  'auth/registerWithEmail',
  async (payload: any, thunkApi) => {
    const res = await registerWithEmail(payload);
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

export const checkLoggedInUser = createAsyncThunk(
  'auth/checkLoggedIn',
  async (_, thunkApi) => {
    const res = await checkLoggedIn();
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

export const logoutUser = createAsyncThunk(
  'auth/logout',
  async (_, thunkApi) => {
    const res = await logout();
    if (res.isSuccessful) {
      return true;
    } else {
      return thunkApi.rejectWithValue(false);
    }
  }
);

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // builder.addCase(["Method Name"].fulfilled, (state, action) => {});
    builder.addCase(loginUserWithEmail.fulfilled, (state, action) => {
      state.user = action.payload.data;
      state.loggedIn = true;
    });
    builder.addCase(registerUserWithEmail.fulfilled, (state, action) => {
      state.user = action.payload.data;
      state.loggedIn = true;
    });
    builder.addCase(checkLoggedInUser.fulfilled, (state, action) => {
      state.user = action.payload.data;
      state.loggedIn = true;
    });
    builder.addCase(logoutUser.fulfilled, (state, action) => {
      state.user = null;
      state.loggedIn = false;
    });
  },
});

export default authSlice.reducer;
