import { createSlice } from '@reduxjs/toolkit';

// type
interface alertState {
  infoMessage?: { message: string };
  successMessage?: { message: string };
  errorMessage?: { message: string };
}

const initialState: alertState = {};

const alertsSlice = createSlice({
  name: 'alerts',
  initialState,
  reducers: {
    infoMessage: (state, action) => {
      state.infoMessage = {
        message: action.payload,
      };
    },
    successMessage: (state, action) => {
      state.successMessage = {
        message: action.payload,
      };
    },
    errorMessage: (state, action) => {
      state.errorMessage = {
        message: action.payload,
      };
    },
  },
});

export const { infoMessage, successMessage, errorMessage } =
  alertsSlice.actions;

export default alertsSlice.reducer;
