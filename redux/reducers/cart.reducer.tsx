import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// services
import { addToCart, delFromCart, subFromCart } from '../services/cart.service';

// interface
import { Product } from '../../interface/index';

// type
interface cartState {
  products: {
    product: Product;
    qty: number;
  }[];
}

const initialState: cartState = {
  products: [],
};

// extra reducers
export const addProductToCart = createAsyncThunk(
  'auth/addToCart',
  async (payload: any, thunkApi) => {
    const res = await addToCart(payload);
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

export const subProductFromCart = createAsyncThunk(
  'auth/subFromCart',
  async (payload: any, thunkApi) => {
    const res = await subFromCart(payload);
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

export const delProductFromCart = createAsyncThunk(
  'auth/delFromCart',
  async (payload: any, thunkApi) => {
    const res = await delFromCart(payload);
    if (res.isSuccessful) {
      return { data: res.data, error: null };
    } else {
      return thunkApi.rejectWithValue({ data: null, error: res.error });
    }
  }
);

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // builder.addCase(["Method Name"].fulfilled, (state, action) => {});
    builder.addCase(addProductToCart.fulfilled, (state, action) => {
      const product = state.products.find(
        (p) => p.product.name === action.payload.data.product.name
      );
      // if product already present
      if (product?.qty) {
        state.products.forEach((p) => {
          if (p.product.name === action.payload.data.product.name) {
            p.qty += action.payload.data.qty;
          }
        });
      }
      // if new product is added
      else {
        state.products.push(action.payload.data);
      }
    });
    builder.addCase(subProductFromCart.fulfilled, (state, action) => {
      state.products.forEach((p) => {
        if (p.product.name === action.payload.data.product.name) {
          p.qty -= action.payload.data.qty;
        }
      });
    });
    builder.addCase(delProductFromCart.fulfilled, (state, action) => {
      state.products = state.products.filter((p) => p.product.name !== action.payload.data.product.name);
    });
  },
});

export default authSlice.reducer;
