import { GetServerSideProps, GetServerSidePropsContext } from 'next';

export function isLoggedIn(gssp: GetServerSideProps) {
  return async (ctx: GetServerSidePropsContext) => {
    const { req } = ctx;

    if (req.headers.cookie) {
      const tokens = req.headers.cookie.split(';');
      const token = tokens.find((token) => token.includes('token'));

      if (!token) {
        return {
          redirect: {
            permanent: false,
            destination: `/auth/login?redirect=${ctx.resolvedUrl}`,
          },
        };
      }
    }
    else{
      return {
        redirect: {
          permanent: false,
          destination: `/auth/login?redirect=${ctx.resolvedUrl}`,
        },
      };
    }

    return await gssp(ctx);
  };
}

export function isLoggedOut(gssp: GetServerSideProps) {
  return async (ctx: GetServerSidePropsContext) => {
    const { req } = ctx;

    if (req.headers.cookie) {
      const tokens = req.headers.cookie.split(';');
      const token = tokens.find((token) => token.includes('token'));

      if (token) {
        return {
          redirect: {
            permanent: false,
            destination: `/`,
          },
        };
      }
    }

    return await gssp(ctx);
  };
}
