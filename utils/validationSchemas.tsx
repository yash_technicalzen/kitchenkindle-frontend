import * as yup from 'yup';
import 'yup-phone';

export const loginSchema = yup.object().shape({
  email: yup.string().required().email().label('Email Address'),
  password: yup.string().required().min(8).label('Password'),
});

export const registerSchema = yup.object().shape({
  firstName: yup.string().required().min(3).label('First Name'),
  lastName: yup.string().required().min(3).label('Last Name'),
  email: yup.string().required().email().label('Email Address'),
  password: yup.string().required().min(8).label('Password'),
});

export const updateProfileSchema = yup.object().shape({
  firstName: yup.string().required().min(3).label('First Name'),
  lastName: yup.string().min(3).label('Last Name'),
  email: yup.string().required().email().label('Email Address'),
  mobile: yup.string().phone().required(),
});

export const updatePasswordSchema = yup.object().shape({
  oldPassword: yup.string().required().min(8).label('Old Password'),
  newPassword: yup.string().required().min(8).label('New Password'),
  confirmNewPassword: yup
    .string()
    .required()
    .oneOf([yup.ref('newPassword'), null], 'Passwords must match')
    .label("Confirm New Password"),
});
