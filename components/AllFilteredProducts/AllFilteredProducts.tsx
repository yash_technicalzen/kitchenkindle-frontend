import React from 'react';
// style
import styles from '../../styles/AllFilteredProducts.module.css';
// data
import { products } from '../../assets/data/index';
// components
import { ProductCard } from '..';

const AllFilteredProducts = () => {
  return (
    <div className="w-100 ml-8 my-14">
      <div className="text-3xl">5 Items found</div>
      <div className="my-10 grid grid-cols-3 gap-10">
        <div>
          <ProductCard size="small" />
        </div>
        <div>
          <ProductCard size="small" />
        </div>
        <div>
          <ProductCard size="small" />
        </div>
        <div>
          <ProductCard size="small" />
        </div>
        <div>
          <ProductCard size="small" />
        </div>
      </div>
    </div>
  );
};

export default AllFilteredProducts;
