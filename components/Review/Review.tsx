import Image from 'next/image';
import React from 'react';
// data
import user from '../../assets/images/user.png';
import { products } from '../../assets/data';
// material ui icons
import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';

const Review = () => {
  const review = products[0].reviews[0];
  return (
    <div className="py-8 border-b-2 border-gray-400">
      <div className="mb-10">
        <div className="flex items-center">
          <div className="mr-8">
            <Image src={user} alt="User" />
          </div>
          <div className="text-2xl">{review.user}</div>
        </div>
      </div>
      <div className="text-3xl flex items-end mb-10">
        {[...Array(5)].map((x, idx) =>
          idx < review.rating ? (
            <StarIcon key={Math.random() * 999999} sx={{ fontSize: 28 }} />
          ) : (
            <StarOutlineIcon
              key={Math.random() * 999999}
              sx={{ fontSize: 28 }}
            />
          )
        )}
      </div>
      <div className="text-2xl font-semibold mb-10">{review.title}</div>
      <div className="text-2xl font-thin mb-10">{review.description}</div>
    </div>
  );
};

export default Review;
