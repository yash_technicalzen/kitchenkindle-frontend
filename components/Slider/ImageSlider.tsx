import Image from 'next/image';
import React, { FunctionComponent, LegacyRef, Ref, useRef } from 'react';
import Slider from 'react-slick';

// styles
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// type
type Props = {
  images: Array<StaticImageData>;
  settings: Object;
  sliderRef?: LegacyRef<Slider> | null;
};

const ImageSlider: FunctionComponent<Props> = (props) => {
  return (
    <Slider {...props.settings} ref={props.sliderRef}>
      {props.images.map((image) => (
        <div key={Math.random() * 100000}>
          <Image src={image} alt="Banner" />
        </div>
      ))}
    </Slider>
  );
};

export default ImageSlider;
