import React, { FunctionComponent } from 'react';
// styles
import styles from '../../styles/OrderStatus.module.css';
// material ui icons
import DoneIcon from '@mui/icons-material/Done';
// type
type Props = {
  status: string;
};

const OrderStatus: FunctionComponent<Props> = ({ status }) => {
  const orderStatus =
    status.toLowerCase() === 'Order Placed'.toLowerCase()
      ? 1
      : status.toLowerCase() === 'Shipped'.toLowerCase()
      ? 2
      : status.toLowerCase() === 'On the Way'.toLowerCase()
      ? 3
      : 4;

  return (
    <div>
      <div>
        <div className="flex items-center">
          <div className="flex flex-col justify-center items-center">
            <div
              className={`${styles.statusCircle} ${
                orderStatus >= 1 && styles.full
              }`}
            >
              <DoneIcon fontSize="large" />
            </div>
            <div className={styles.statusText}>Order Placed</div>
          </div>
          <div
            className={`${styles.statusLine} ${
              orderStatus == 1 ? styles.half : orderStatus > 1 && styles.full
            }`}
          ></div>
          <div className="flex flex-col justify-center items-center">
            <div
              className={`${styles.statusCircle} ${
                orderStatus >= 2 && styles.full
              }`}
            >
              <DoneIcon fontSize="large" />
            </div>
            <div className={styles.statusText}>Shipped</div>
          </div>
          <div
            className={`${styles.statusLine} ${
              orderStatus == 2 ? styles.half : orderStatus > 2 && styles.full
            }`}
          ></div>
          <div className="flex flex-col justify-center items-center">
            <div
              className={`${styles.statusCircle} ${
                orderStatus >= 3 && styles.full
              }`}
            >
              <DoneIcon fontSize="large" />
            </div>
            <div className={styles.statusText}>On the Way</div>
          </div>
          <div
            className={`${styles.statusLine} ${
              orderStatus == 3 ? styles.half : orderStatus > 3 && styles.full
            }`}
          ></div>
          <div className="flex flex-col justify-center items-center">
            <div
              className={`${styles.statusCircle} ${
                orderStatus >= 4 && styles.full
              }`}
            >
              <DoneIcon fontSize="large" />
            </div>
            <div className={styles.statusText}>Delivered</div>
          </div>
        </div>
        <div></div>
      </div>
    </div>
  );
};

export default OrderStatus;
