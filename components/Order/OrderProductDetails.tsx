import React, { FunctionComponent, useEffect, useMemo, useState } from 'react';
// styles
import styles from '../../styles/OrderProductDetails.module.css';
// data
import Image from 'next/image';
// interface
import { Product } from '../../interface/index';
// types
type ProductProps = {
  product: Product;
  qty: number;
};

type PriceProps = {
  title: string;
  value: number;
};

type OrderProductDetailsProps = {
  products: {
    product: Product;
    qty: number;
  }[];
  shipping?: number;
  discount?: number;
};

const OrderProductDetails: FunctionComponent<OrderProductDetailsProps> = ({
  products,
  shipping = 0,
  discount = 0,
}) => {
  // state
  const [subtotal, setSubtotal] = useState(0);

  // function
  useEffect(() => {
    let subtotal = 0;
    products.forEach(({ product, qty }) => {
      subtotal += product.price * (qty || 1);
    });
    setSubtotal(subtotal);
  }, [products]);

  const ProductDisplay: FunctionComponent<ProductProps> = ({
    product,
    qty,
  }) => {
    return (
      <div className="w-full flex justify-between items-center mt-10">
        <div className={styles.productImageBox}>
          <Image src={product.mainImg} alt="Product" />
        </div>
        <div className={styles.productDetails}>
          <div className={styles.productName}>{product.name}</div>
          <div className={styles.productCalc}>
            {qty} x Rs.{product.price}.00
          </div>
        </div>
        <div className={styles.productSubtotal}>
          Rs.{product.price * (qty ? qty : 1)}.00
        </div>
      </div>
    );
  };

  const PriceComponent: FunctionComponent<PriceProps> = ({ title, value }) => {
    return (
      <div className="w-100 flex justify-between items-center mt-10">
        <div className={styles.priceTitle}>{title}</div>
        <div className={styles.priceValue}>Rs. {value}.00</div>
      </div>
    );
  };
  return (
    <div className={styles.orderProductDetails}>
      <div className="flex justify-between items-center">
        <div className="text-3xl">Product</div>
        <div className="text-3xl">Subtotal</div>
      </div>
      {products.map(({ product, qty }) => (
        <div key={Math.random() * 999999}>
          <ProductDisplay product={product} qty={qty} />
          <div className={styles.line}></div>
        </div>
      ))}
      <PriceComponent title="Subtotal" value={subtotal} />
      <div className={styles.line}></div>
      <PriceComponent title="Shipping" value={shipping} />
      <div className={styles.line}></div>
      <PriceComponent title="Discount" value={discount} />
      <div className={styles.line}></div>
      <PriceComponent title="Total" value={subtotal + shipping - discount} />
    </div>
  );
};

export default OrderProductDetails;
