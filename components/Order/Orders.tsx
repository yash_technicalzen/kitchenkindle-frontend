import React, { useState } from 'react';
// components
import { Button, OrderDetails, SearchBar, OrdersTable } from '..';
// styles
import styles from '../../styles/Orders.module.css';
// data
import pImg from '../../assets/images/pImage.png';
import { products } from '../../assets/data/index';
// interface
import { Product } from '../../interface/index';
// types
interface Order {
  ono: string;
  odate: string;
  status: string;
  dtime: string;
  mop: string;
  oprice: string;
  address?: {
    type: string;
    address: string;
    area: string;
    city: string;
    pincode: string;
    landmark: string;
  };
  paymentStatus?: string;
  products: {
    product: Product;
    qty: number;
  }[];
  shippingPrice?: number;
  discount?: number;
  total?: number;
}

const Orders = () => {
  // state
  const [selectedOrder, setSelectedOrder] = useState<Order | null>(null);
  const data: Order[] = [
    {
      ono: 'ABCDEFGH',
      odate: '12/05/2022',
      status: 'Order Placed',
      dtime: '15/05/2022',
      mop: 'Cash on Delivery',
      oprice: 'Rs. 345.00',
      address: {
        type: 'Home',
        address: 'Pocket F Dilshad Garden',
        area: 'Dilshad Garden',
        city: 'Delhi',
        pincode: '110032',
        landmark: 'Near GTB Hospital',
      },
      paymentStatus: 'Pending',
      products: [
        { product: products[0], qty: 2 },
        { product: products[0], qty: 2 },
      ],
    },
    {
      ono: 'ABCDEFGH',
      odate: '12/05/2022',
      status: 'On the Way',
      dtime: '15/05/2022',
      mop: 'UPI',
      oprice: 'Rs. 345.00',
      address: {
        type: 'Home',
        address: 'Pocket F Dilshad Garden',
        area: 'Dilshad Garden',
        city: 'Delhi',
        pincode: '110032',
        landmark: 'Near GTB Hospital',
      },
      paymentStatus: 'Completed',
      products: [
        { product: products[0], qty: 3 },
        { product: products[0], qty: 3 },
      ],
    },
  ];

  // function
  return (
    <div className={`w-full ${styles.orders}`}>
      {!selectedOrder ? (
        <>
          <div className="flex justify-between items-center">
            <div className={styles.ordersTitle}>My Orders</div>
            <SearchBar
              className={styles.orderFilterSearch}
              placeholder="Search Order List.."
            />
          </div>
          <div className="mt-16">
            {data.length === 0 ? (
              <h1 className="text-3xl text-center my-6">
                There are no order yet
              </h1>
            ) : (
              <OrdersTable data={data} setSelectedOrder={setSelectedOrder} />
            )}
          </div>
        </>
      ) : (
        <>
          <Button text="Go Back" onClick={() => setSelectedOrder(null)} />
          <br />
          <br />
          <br />
          <br />
          <div className={`mb-14 ${styles.ordersTitle}`}>Order Details</div>
          <OrderDetails order={selectedOrder} />
        </>
      )}
    </div>
  );
};

export default Orders;
