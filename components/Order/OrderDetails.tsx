import { integerPropType } from '@mui/utils';
import React, { FunctionComponent } from 'react';
// styles
import styles from '../../styles/OrderDetails.module.css';
// components
import { OrderStatus, OrderProductDetails } from '..';
// interface
import { Product } from '../../interface/index';
//types
interface Order {
  ono: string;
  odate: string;
  status: string;
  dtime: string;
  mop: string;
  oprice: string;
  address?: {
    type: string;
    address: string;
    area: string;
    city: string;
    pincode: string;
    landmark: string;
  };
  paymentStatus?: string;
  products: {
    product: Product;
    qty: number;
  }[];
  shippingPrice?: number;
  discount?: number;
  total?: number;
}

type Props = {
  order: Order;
};

const OrderDetails: FunctionComponent<Props> = ({ order }) => {
  return (
    <div className="w-full flex">
      <div className="w-[50%] mr-8">
        <div className="">
          <div className="text-3xl mb-8">Order Status</div>
          <OrderStatus status={order.status} />
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Order Number</div>
          <div className={styles.orderDetailField}>{order.ono}</div>
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Delivery Address</div>
          <div className={styles.orderDetailField}>
            <div className={styles.orderAddressType}>{order.address?.type}</div>
            <div className={styles.orderAddressAddress}>
              {order.address?.address}
            </div>
            <div className="w-full flex">
              <div className={`w-[50%] ${styles.orderAddressSub}`}>
                Area: <span>{order.address?.area}</span>
              </div>
              <div className={`w-[50%] ${styles.orderAddressSub}`}>
                Landmark: <span>{order.address?.landmark}</span>
              </div>
            </div>
            <div className="w-full flex">
              <div className={`w-[50%] ${styles.orderAddressSub}`}>
                City: <span>{order.address?.city}</span>
              </div>
              <div className={`w-[50%] ${styles.orderAddressSub}`}>
                Pincode: <span>{order.address?.pincode}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Order Date</div>
          <div className={styles.orderDetailField}>{order.odate}</div>
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Delivery Time</div>
          <div className={styles.orderDetailField}>{order.dtime}</div>
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Payment Status</div>
          <div className={`flex items-center  ${styles.orderDetailField}`}>
            <div
              className={`${
                order.paymentStatus !== 'Pending' && styles.completed
              } ${styles.paymentStatus}`}
            ></div>
            {order.paymentStatus}
          </div>
        </div>
        <div className="mt-20">
          <div className="text-3xl mb-8">Payment Mode</div>
          <div className={styles.orderDetailField}>{order.mop}</div>
        </div>
      </div>
      <div className="w-[50%] ml-8">
        <OrderProductDetails
          shipping={0}
          discount={0}
          products={order.products}
        />
      </div>
    </div>
  );
};

export default OrderDetails;
