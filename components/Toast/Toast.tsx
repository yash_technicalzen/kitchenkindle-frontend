import React from 'react';
import { toast } from 'react-toastify';

// type
type Props = {
  type: 'success' | 'info' | 'error' | 'warn';
  message: string;
};

const MessageContainer = ({ type, message }: Props) => {
  return (
    <div style={{ display: 'flex', color: 'white' }}>
      <div style={{ flexGrow: 1, fontSize: 15, padding: '8px 12px' }}>
        {message}
      </div>
    </div>
  );
};

const ToastMessage = ({ type, message }: Props) => {
  switch (type) {
    case 'info':
      return toast.info(<MessageContainer type={type} message={message} />);
    case 'success':
      return toast.success(<MessageContainer type={type} message={message} />);
    case 'error':
      return toast.error(<MessageContainer type={type} message={message} />);
    case 'warn':
      return toast.warn(<MessageContainer type={type} message={message} />);
    default:
      return toast(<MessageContainer type={type} message={message} />);
  }
};

export default ToastMessage;
