import React, { FunctionComponent } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';

// hooks
import { useAppDispatch } from '../../redux/hooks';

// material ui icons
import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import AddIcon from '@mui/icons-material/Add';

// styles
import styles from '../../styles/ProductCard.module.css';

// data
import vegSmall from '../../assets/images/vegSmall.png';
import vegLarge from '../../assets/images/vegLarge.png';
import nonvegSmall from '../../assets/images/nonvegSmall.png';
import nonvegLarge from '../../assets/images/nonvegLarge.png';
import { products } from '../../assets/data/index';

// reducers
import { addProductToCart } from '../../redux/reducers/cart.reducer';
import {
  errorMessage,
  successMessage,
} from '../../redux/reducers/alert.reducer';

// interface
import { Product } from '../../interface/index';

type Props = {
  className?: string;
  product?: Product;
  size: string;
};

const ProductCard: FunctionComponent<Props> = ({
  className,
  product = products[0],
  size,
}) => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  // state

  // function
  async function handleAddToCart() {
    const res: any = await dispatch(addProductToCart({ product, qty: 1 }));
    if (res.payload.error) {
      dispatch(errorMessage(res.payload.error));
    } else {
      dispatch(successMessage('Product Added to Cart'));
    }
  }
  return (
    <div
      className={`${className} ${
        size == 'large' ? styles.productLargeCard : styles.productSmallCard
      }`}
    >
      <div
        className={styles.productCardImage}
        onClick={() =>
          router.push(
            `/products/${product.name.toLowerCase().split(' ').join('-')}`
          )
        }
      >
        <Image src={product.mainImg} alt="Product" layout="fill" />
      </div>
      <div
        className={
          size == 'large'
            ? styles.productCardLargeDetails
            : styles.productCardSmallDetails
        }
      >
        <div
          className={
            size == 'large' ? styles.productLargeName : styles.productSmallName
          }
          onClick={() =>
            router.push(
              `/products/${product.name.toLowerCase().split(' ').join('-')}`
            )
          }
        >
          {product.name}
        </div>
        <div className="flex py-2 items-center">
          <div
            className={
              size == 'large'
                ? styles.productLargeType
                : styles.productSmallType
            }
          >
            {product.type == 'veg' ? (
              <Image src={size == 'large' ? vegLarge : vegSmall} alt="Veg" />
            ) : (
              <Image
                src={size == 'large' ? nonvegLarge : nonvegSmall}
                alt="Nonveg"
              />
            )}
          </div>
          <div
            className={
              size == 'large'
                ? styles.productLargeRating
                : styles.productSmallRating
            }
          >
            {[...Array(5)].map((x, idx) =>
              idx < product.avgRating ? (
                <StarIcon
                  key={Math.random() * 999999}
                  fontSize={size == 'large' ? 'large' : 'small'}
                />
              ) : (
                <StarOutlineIcon
                  key={Math.random() * 999999}
                  fontSize={size == 'large' ? 'large' : 'small'}
                />
              )
            )}
            {`(${product.totalRatings})`}
          </div>
        </div>
        <div className="flex items-center">
          <div
            className={
              size == 'large' ? styles.productLargeMrp : styles.productSmallMrp
            }
          >
            <del>Rs. {product.mrp}.00</del>
          </div>
          <div
            className={
              size == 'large'
                ? styles.productLargePrice
                : styles.productSmallPrice
            }
          >
            Rs. {product.price}.00
          </div>
        </div>
        <div className={styles.addToCartCta} onClick={handleAddToCart}>
          +
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
