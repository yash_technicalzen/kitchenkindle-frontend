import React, { useState } from 'react';
// style
import styles from '../../styles/Checkout.module.css';
// material ui components
import { Stepper, Step, StepLabel, StepContent } from '@mui/material';
// components
import { DeliverySchedule, ShippingAddressForm } from '..';

const Checkout = () => {
  // state
  const [activeStep, setActiveStep] = useState(0);
  // function
  return (
    <div className="mt-6 mb-12">
      <Stepper activeStep={activeStep} orientation="vertical">
        <Step>
          <StepLabel
            className="cursor-pointer"
            onClick={() => setActiveStep(0)}
          >
            Shipping Address
          </StepLabel>
          <StepContent>
            <ShippingAddressForm />
          </StepContent>
        </Step>
        <Step>
          <StepLabel
            className="cursor-pointer"
            onClick={() => setActiveStep(1)}
          >
            Delivery Schedule
          </StepLabel>
          <StepContent>
            <DeliverySchedule />
          </StepContent>
        </Step>
      </Stepper>
    </div>
  );
};

export default Checkout;
