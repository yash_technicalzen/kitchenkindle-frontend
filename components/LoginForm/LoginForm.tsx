import React from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { loginSchema } from '../../utils/validationSchemas';

// hooks
import { useAppDispatch } from '../../redux/hooks';

// reducers
import { loginUserWithEmail } from '../../redux/reducers/auth.reducer';
import {
  errorMessage,
  successMessage,
} from '../../redux/reducers/alert.reducer';

// components
import { Button } from '../../components';

// material ui components
import { TextField } from '@mui/material';

// styles
import Classes from '../../styles/Form.module.css';

// data
import logo from '../../assets/images/logo.png';

const LoginForm = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  // state

  // form
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(loginSchema) });

  // function
  const handleLogin = async (formData: any) => {
    const res: any = await dispatch(loginUserWithEmail(formData));
    if (res.payload.error) {
      dispatch(errorMessage(res.payload.error));
    } else {
      dispatch(successMessage('Logged In Successfully'));
      if (router.query?.redirect) {        
        router.push(router.query.redirect.toString());
      } else {
        router.push('/dashboard/user/1');
      }
    }
  };

  return (
    <div className={Classes.loginForm}>
      <div className={Classes.formLogo}>
        <Image src={logo} alt="Form Logo" />
      </div>
      <div className={Classes.formTitle}>Welcome Back</div>
      <div className={Classes.formSubtitle}>
        {/* &apos; id used for ' */}
        Don&apos;t have an account?{' '}
        <span
          onClick={(e) => {
            if (router.query?.redirect) {
              router.push(
                '/auth/register?redirect=' + router.query?.redirect[0]
              );
            } else {
              router.push('/auth/register');
            }
          }}
        >
          Create Account
        </span>
      </div>
      <div className="mt-10 w-[80%]">
        <form className="w-full" onSubmit={handleSubmit(handleLogin)}>
          <TextField
            id="login_email"
            label="Email Address"
            variant="outlined"
            {...register('email')}
            size="medium"
            margin="normal"
            error={errors.email?.message === undefined ? false : true}
            helperText={errors.email?.message}
            fullWidth
          />
          <TextField
            id="login_password"
            label="Password"
            variant="outlined"
            {...register('password')}
            size="medium"
            type={'password'}
            margin="normal"
            error={errors.password?.message === undefined ? false : true}
            helperText={errors.password?.message}
            fullWidth
          />
          <h3 className={Classes.forgotPassword}>Forgot Password?</h3>
          <Button text="Login" width="full" type="submit" />
        </form>
      </div>
    </div>
  );
};

export default LoginForm;
