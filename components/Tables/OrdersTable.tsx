import React, { FunctionComponent } from 'react';
import Image from 'next/image';
// data
import more from '../../assets/images/more.png';
// data table
import DataTable, {
  TableColumn,
  createTheme,
} from 'react-data-table-component';
// interface
import { Product } from '../../interface/index';
//types
interface Order {
  ono: string;
  odate: string;
  status: string;
  dtime: string;
  mop: string;
  oprice: string;
  address?: {
    type: string;
    address: string;
    area: string;
    city: string;
    pincode: string;
    landmark: string;
  };
  paymentStatus?: string;
  products:{
    product: Product,
    qty: number,
  }[];
  shippingPrice?: number;
  discount?: number;
  total?: number;
}

type Props = {
  data: Order[];
  setSelectedOrder: (row: Order) => void;
};

const customStyles = {
  rows: {
    style: {},
  },
  headCells: {
    style: {
      backgroundColor: 'var(--orange)',
      color: 'var(--white)',
      padding: '0 2rem',
    },
  },
  cells: {
    style: {
      padding: '0 2rem',
    },
  },
};

createTheme('orange', {
  background: {
    default: 'transparent',
  },
});

const OrdersTable: FunctionComponent<Props> = ({ data, setSelectedOrder }) => {
  const columns: TableColumn<Order>[] = [
    {
      name: 'Order Number',
      selector: (row) => row.ono,
      minWidth: '12rem',
    },
    {
      name: 'Order Date',
      selector: (row) => row.odate,
      minWidth: '12rem',
    },
    {
      name: 'Order Status',
      minWidth: '15rem',
      cell: (row) => {
        let color = 'grey';
        if (row.status === 'Order Places') color = 'grey';
        else if (row.status === 'Shipped') color = 'orange';
        else if (row.status == 'On the Way') color = 'yellow';
        else if (row.status === 'Delivered') color = 'green';
        return (
          <div className="flex justify-center items-center">
            <div
              style={{
                marginRight: '.9rem',
                width: '1rem',
                height: '1rem',
                borderRadius: '100%',
                backgroundColor: color,
              }}
            ></div>
            {row.status}
          </div>
        );
      },
    },
    {
      name: 'Delivery By',
      selector: (row) => row.dtime,
      minWidth: '12rem',
    },
    {
      name: 'Mode of Payment',
      selector: (row) => row.mop,
      minWidth: '18rem',
    },
    {
      name: 'Order Price',
      selector: (row) => row.oprice,
      minWidth: '12rem',
    },
    {
      cell: (row) => (
        <button className="relative" onClick={() => setSelectedOrder(row)}>
          <Image src={more} alt="More" width="35%" height="35%" />
        </button>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
      minWidth: '5rem',
    },
  ];
  return (
    <DataTable
      responsive={true}
      columns={columns}
      data={data}
      theme="orange"
      customStyles={customStyles}
      pagination
    />
  );
};

export default OrdersTable;
