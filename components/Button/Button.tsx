import Image from 'next/image';
import React, { FunctionComponent } from 'react';

// styles
import Classes from '../../styles/Button.module.css';

// types
type Props = {
  text: string;
  color?: string;
  size?: 'sm' | 'md' | 'lg';
  width?: 'auto' | 'full';
  type?: 'button' | 'submit' | 'reset';
  disabled?: boolean;
  className?: string;
  prevIcon?: StaticImageData;
  nextIcon?: StaticImageData;
  onClick?: () => void;
};

const Button: FunctionComponent<Props> = ({
  text,
  color,
  size,
  width,
  type,
  className,
  disabled,
  prevIcon,
  nextIcon,
  onClick,
}) => {
  return (
    <button
      onClick={onClick}
      type={type ? type : 'button'}
      className={`${width === 'full' && 'w-full'} ${
        Classes.orangeButton
      } ${className} ${disabled && Classes.disabled}`}
      disabled={disabled}
    >
      {prevIcon && (
        <div className="mr-6 flex justify-center items-center">
          <Image src={prevIcon} alt="Icon" />
        </div>
      )}
      {text}
      {nextIcon && (
        <div className="ml-6 flex justify-center items-center">
          <Image src={nextIcon} alt="Icon" />
        </div>
      )}
    </button>
  );
};

export default Button;
