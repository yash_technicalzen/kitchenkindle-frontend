import React from 'react';
import { useRouter } from 'next/router';

// styles
import Classes from '../../styles/YouMayAlsoLike.module.css';
import { ProductCard } from '..';

const YouMayAlsoLike = () => {
  const router = useRouter();
  // states
  return (
    <div>
      <div className="flex justify-between items-center my-20">
        <h1 className={Classes.productGridTitle}>You may also like</h1>
      </div>
      <div className={Classes.productGridContainer}>
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
      </div>
    </div>
  );
};

export default YouMayAlsoLike;
