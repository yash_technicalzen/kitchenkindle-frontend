import React, { FunctionComponent } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
// styles
import styles from '../../styles/PageBanner.module.css';
// data
import homeIcon from '../../assets/images/homeIcon.png';
import keyboard_arrow_right from '../../assets/images/keyboard_arrow_right.png';
// type
type Props = {
  title: string;
  breadcrumbs: {
    title: string;
    route: string;
  }[];
};

const PageBanner: FunctionComponent<Props> = ({ title, breadcrumbs }) => {
  const router = useRouter();
  return (
    <div className={styles.pageBanner}>
      <div className="w-[60%] text-center">
        <div className={styles.pageBannerTitle}>{title}</div>
        <div className={styles.pageBannerBreadcrumbs}>
          <div className={styles.pageBannerBreadcrumb}>
            <Image src={homeIcon} alt="Home Icon" />
            <div
              className="mr-3 ml-2 cursor-pointer"
              onClick={(e) => router.push('/')}
            >
              Home
            </div>
          </div>
          {breadcrumbs.map((breadcrumb) => (
            <div
              className={styles.pageBannerBreadcrumb}
              key={Math.random() * 999999}
            >
              <Image src={keyboard_arrow_right} alt="Right Arrow" />
              <div
                className="mx-3 cursor-pointer"
                onClick={(e) => router.push(breadcrumb.route)}
              >
                {breadcrumb.title}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PageBanner;
