import Image from 'next/image';
import React, { useState } from 'react';
// style
import styles from '../../styles/ProductOverview.module.css';
// data
import { products } from '../../assets/data/index';
import vegLarge from '../../assets/images/vegLarge.png';
import nonvegLarge from '../../assets/images/nonvegLarge.png';
import share from '../../assets/images/share.png';
import favourite from '../../assets/images/favourite.png';
import shopping_bag_white from '../../assets/images/shopping_bag_white.png';
import tags from '../../assets/images/tags.png';
// interface
import { Product } from '../../interface/index';
// material ui icons
import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
// material ui components
import { TextField } from '@mui/material';
import { Button } from '..';

const ProductOverview = () => {
  // state
  const [qty, setQty] = useState(1);
  const [pincode, setPincode] = useState('');
  const product: Product = products[0];
  // function
  const handleQtyChange = (increase: boolean) => {
    if (increase) setQty(qty + 1);
    else setQty(qty - 1);
  };
  return (
    <div className="grid grid-cols-5 gap-8 mb-14">
      <div className={`col-span-3 ${styles.productImage}`}>
        <Image src={product.mainImg} alt="Product" layout="fill" />
      </div>
      <div className={`col-span-2 ml-8 ${styles.productOverview}`}>
        <div className={styles.productOverviewTitle}>{product.name}</div>
        <div className="flex py-2 items-end mb-10">
          <div className="mr-8">
            {product.type == 'veg' ? (
              <Image src={vegLarge} alt="Veg" />
            ) : (
              <Image src={nonvegLarge} alt="Nonveg" />
            )}
          </div>
          <div className="text-3xl flex items-end">
            {[...Array(5)].map((x, idx) =>
              idx < product.avgRating ? (
                <StarIcon key={Math.random() * 999999} sx={{ fontSize: 28 }} />
              ) : (
                <StarOutlineIcon
                  key={Math.random() * 999999}
                  sx={{ fontSize: 28 }}
                />
              )
            )}
            {`(${product.totalRatings})`}
          </div>
        </div>
        <div className="flex items-center mb-10">
          <div className={`${styles.productOverviewMrp}`}>
            <del>Rs. {product.mrp}.00</del>
          </div>
          <div className={`ml-10 ${styles.productOverviewPrice}`}>
            Rs. {product.price}.00
          </div>
        </div>
        <div className={`mb-10 ${styles.productOverviewQty}`}>
          <div
            onClick={() => qty !== 1 && handleQtyChange(false)}
            className={`${styles.productOverviewQtyCta} ${
              qty === 1 && styles.disabled
            }`}
          >
            -
          </div>
          {qty}
          <div
            onClick={() => handleQtyChange(true)}
            className={`${styles.productOverviewQtyCta} `}
          >
            +
          </div>
        </div>
        <div className="text-3xl font-semibold">Check Availability</div>
        <div className="flex items-center">
          <div className="w-[65%]">
            <TextField
              id="check_pincode"
              label="Enter Pincode"
              variant="outlined"
              size="medium"
              margin="normal"
              type={'number'}
              value={pincode}
              onChange={(e) => setPincode(e.target.value)}
              fullWidth
            />
          </div>
          <div className="w-[35%] ml-4 h-100">
            <Button text="Check" width="full" className={styles.checkBtn} />
          </div>
        </div>
        <div className="grid grid-cols-2 gap-6">
          <div className="mt-4">
            <Button
              disabled={true}
              text="Add to Cart"
              prevIcon={shopping_bag_white}
              width="full"
            />
          </div>
          <div className="mt-4">
            <Button
              disabled={true}
              text="Buy Now"
              className={styles.whiteBtn}
              width="full"
            />
          </div>
          <div className="mt-4">
            <Button
              text="Add to Wishlist"
              className={styles.outlinedBtn}
              prevIcon={favourite}
              width="full"
            />
          </div>
          <div className="mt-4">
            <Button
              text="Share"
              className={styles.outlinedBtn}
              prevIcon={share}
              width="full"
            />
          </div>
        </div>
        <div className={styles.productOverviewTags}>
          <div className="mr-4 flex items-center justify-center">
            <Image src={tags} alt="Tag" />
          </div>
          Tags:
          <div className="ml-8 w-[65%] flex justify-start items-center flex-wrap">
            {product.tags.map((tag) => (
              <div
                key={Math.random() * 999999}
                className={styles.productOverviewTag}
              >
                {tag}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductOverview;
