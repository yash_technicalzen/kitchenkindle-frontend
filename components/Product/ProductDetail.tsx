import React from 'react';
// style
import styles from '../../styles/ProductDetail.module.css';
// data
import { products } from '../../assets/data/index';
// interface
import { Product } from '../../interface';

const ProductDetail = () => {
  const product: Product = products[0];
  return (
    <div>
      <div
        className={`text-2xl flex items-center mb-16`}
      >
        <div className="font-bold mr-6">Servings:</div>{' '}
        {product.details?.serving}
      </div>
      <div
        className={`text-2xl flex items-center mb-16  `}
      >
        {product.details?.description}
      </div>
      <div
        className={`text-2xl flex flex-col justify-center mb-16  `}
      >
        <div className="font-bold mr-6 mb-10">
          *In the kit* (Items included in the kit)
        </div>
        <div className="flex flex-col ml-10">
          {product.details?.inKit.map((item) => (
            <li className="my-4" key={Math.random() * 98999999}>
              {item}
            </li>
          ))}
        </div>
      </div>
      <div
        className={`text-2xl flex flex-col justify-center mb-16  `}
      >
        <div className="font-bold mr-6 mb-10">*From your pantry*</div>
        <div className="flex flex-col ml-10">
          {product.details?.fromPantry.map((item) => (
            <li className="my-4" key={Math.random() * 98999999}>
              {item}
            </li>
          ))}
        </div>
      </div>
      <div
        className={`text-2xl flex items-center mb-16`}
      >
        <div className="font-bold mr-6">Cook Time:</div>{' '}
        {product.details?.cookingTime}
      </div>
      <div
        className={`text-2xl flex items-center mb-16`}
      >
        <div className="font-bold mr-6">Allergens:</div>{' '}
        {product.details?.allergens.map((allergy, index) => (
          <div className="ml-2" key={Math.random() * 9999999}>
            {allergy}
            {index + 1 !== product.details?.allergens.length && ' |'}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductDetail;
