import React from 'react';
// style
import styles from '../../styles/ProductReview.module.css';
// data
import { products } from '../../assets/data/index';
import review from '../../assets/images/review.png';
// interface
import { Product } from '../../interface';
// material ui icons
import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
// material ui components
import { styled } from '@mui/material/styles';
import { LinearProgress, linearProgressClasses } from '@mui/material';
// components
import { Button, Review } from '..';

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 25,
  borderRadius: 5,
  border: '1px solid #9c9c9c',
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor: 'transparent',
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 5,
    backgroundColor: '#f9884b',
  },
}));

const ProductReview = () => {
  const product: Product = products[0];
  return (
    <div className="grid grid-cols-6 gap-12">
      <div className={`col-span-2 flex flex-col`}>
        <div className="text-xl mb-10">Customer Reviews</div>
        <div className="text-3xl flex items-end mb-10">
          {[...Array(5)].map((x, idx) =>
            idx < product.avgRating ? (
              <StarIcon key={Math.random() * 999999} sx={{ fontSize: 28 }} />
            ) : (
              <StarOutlineIcon
                key={Math.random() * 999999}
                sx={{ fontSize: 28 }}
              />
            )
          )}
          <div className="ml-4">{`${product.avgRating} out of 5`}</div>
        </div>
        <div className="text-3xl flex items-center mb-12">
          <div className="font-bold mr-6">{product.totalRatings}</div>
          Global Reviews
        </div>
        <div className="text-3xl flex items-center mb-8">
          <div className="font-bold mr-8">5 Star</div>
          <div className="w-[60%] mr-6">
            <BorderLinearProgress
              variant="determinate"
              value={(product.ratings[5] / product.totalRatings) * 100}
            />
          </div>
          <div>{(product.ratings[5] / product.totalRatings) * 100}%</div>
        </div>
        <div className="text-3xl flex items-center mb-8">
          <div className="font-bold mr-8">4 Star</div>
          <div className="w-[60%] mr-6">
            <BorderLinearProgress
              variant="determinate"
              value={(product.ratings[4] / product.totalRatings) * 100}
            />
          </div>
          <div>{(product.ratings[4] / product.totalRatings) * 100}%</div>
        </div>
        <div className="text-3xl flex items-center mb-8">
          <div className="font-bold mr-8">3 Star</div>
          <div className="w-[60%] mr-6">
            <BorderLinearProgress
              variant="determinate"
              value={(product.ratings[3] / product.totalRatings) * 100}
            />
          </div>
          <div>{(product.ratings[3] / product.totalRatings) * 100}%</div>
        </div>
        <div className="text-3xl flex items-center mb-8">
          <div className="font-bold mr-8">2 Star</div>
          <div className="w-[60%] mr-6">
            <BorderLinearProgress
              variant="determinate"
              value={(product.ratings[2] / product.totalRatings) * 100}
            />
          </div>
          <div>{(product.ratings[2] / product.totalRatings) * 100}%</div>
        </div>
        <div className="text-3xl flex items-center mb-8">
          <div className="font-bold mr-8">1 Star</div>
          <div className="w-[60%] mr-6">
            <BorderLinearProgress
              variant="determinate"
              value={(product.ratings[1] / product.totalRatings) * 100}
            />
          </div>
          <div>{(product.ratings[1] / product.totalRatings) * 100}%</div>
        </div>
        <div className="mt-4">
          <Button
            prevIcon={review}
            text="Write a review"
            className={styles.whiteBtn}
            width="full"
          />
        </div>
      </div>
      <div className={`col-span-4`}>
        <div className="text-xl mb-10">Filter</div>
        <Review />
        <Review />
        <Review />
      </div>
    </div>
  );
};

export default ProductReview;
