import { useRouter } from 'next/router';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
// style
import styles from '../../styles/Breadcrumbs.module.css';
// data
import homeIconBlack from '../../assets/images/homeIconBlack.png';
import keyboard_arrow_right_black from '../../assets/images/keyboard_arrow_right_black.png';

const Breadcrumbs = () => {
  const router = useRouter();
  // state
  const [breadcrumbs, setBreadcrums] = useState<string[]>([]);
  // function
  useEffect(() => {
    setBreadcrums(router.asPath.slice(1).split('/'));
  }, [router]);

  const camelCase = (string: string) => {
    const strArray = string.split('-');

    let str = '';
    strArray.forEach((b) => {
      str = str + b.charAt(0).toUpperCase() + b.slice(1).toLowerCase() + ' ';
    });

    return str;
  };

  return (
    <div className={styles.breadcrumbs}>
      <div className={styles.breadcrumb}>
        <Image src={homeIconBlack} alt="Home Icon" />
        <div
          className="mr-3 ml-2 cursor-pointer"
          onClick={(e) => router.push('/')}
        >
          Home
        </div>
      </div>
      {breadcrumbs.map((b, index) => {
        const breadcrumb = camelCase(b);
        const route = breadcrumbs.slice(0, index + 1).join('/');
        return (
          <div className={styles.breadcrumb} key={Math.random() * 999999}>
            <Image src={keyboard_arrow_right_black} alt="Right Arrow" />
            <div
              className="mx-3 cursor-pointer"
              onClick={(e) => router.push(`/${route}`)}
            >
              {breadcrumb}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Breadcrumbs;
