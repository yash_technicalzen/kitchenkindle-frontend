import React, { FunctionComponent } from 'react';
import Image from 'next/image';
// style
import styles from '../../styles/AddressCard.module.css';
// data
import edit from '../../assets/images/edit.png';
// type
interface Address {
  type: string;
  address: string;
  area: string;
  city: string;
  pincode: string;
  landmark: string;
}

type Props = {
  address: Address;
  editable: Boolean;
  setSelectedAddress: (address: Address) => void;
};

const AddressCard: FunctionComponent<Props> = ({
  address,
  editable,
  setSelectedAddress,
}) => {
  return (
    <div className={styles.addressCard}>
      {editable && (
        <div
          className={styles.edit}
          onClick={(e) => setSelectedAddress(address)}
        >
          <Image src={edit} alt="Edit" />
        </div>
      )}
      <div className={styles.addressType}>{address.type}</div>
      <div className={styles.addressAddress}>{address.address}</div>
      <div className="grid grid-cols-2 gap-6 mt-6">
        <div className={styles.addressSub}>
          <span>Area:</span> {address.area}
        </div>
        <div className={styles.addressSub}>
          <span>Landmark:</span>
          {address.landmark}
        </div>
        <div className={styles.addressSub}>
          <span>City:</span>
          {address.city}
        </div>
        <div className={styles.addressSub}>
          <span>Pincode:</span>
          {address.pincode}
        </div>
      </div>
    </div>
  );
};

export default AddressCard;
