import React, { FunctionComponent, useState } from 'react';
// style
import styles from '../../styles/Address.module.css';
// component
import { AddressCard, Button } from '..';
// type
type AddressType = {
  type: string;
  address: string;
  area: string;
  city: string;
  pincode: string;
  landmark: string;
}

const Address : FunctionComponent = () => {
  // state
  const [selectedAddress, setSelectedAddress] = useState<AddressType| null>(null);

  // function
  const addresses: AddressType[] = [
    {
      type: 'Home',
      address: 'Pocket Q Dilshad Garden New Delhi',
      area: 'Dilshad Garden',
      landmark: 'Near GTB Hospital',
      city: 'Delhi',
      pincode: '110095',
    },
    {
      type: 'Work',
      address: 'Pocket Q Dilshad Garden New Delhi',
      area: 'Dilshad Garden',
      landmark: 'Near GTB Hospital',
      city: 'Delhi',
      pincode: '110095',
    },
  ];
  return (
    <div className={` w-full ${styles.address}`}>
      <div className={styles.addressTitle}>My Address</div>
      <div className="mt-10 grid grid-cols-2 gap-8">
        {addresses.map((address) => (
          <AddressCard
            address={address}
            editable={true}
            setSelectedAddress={setSelectedAddress}
            key={Math.random() * 999999}
          />
        ))}
      </div>
      <div className="mt-8">
        <Button text="Add Address" />
      </div>
    </div>
  );
};

export default Address;
