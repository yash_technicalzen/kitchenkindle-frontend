import Image from 'next/image';
import React from 'react';
// material ui icons
import StarIcon from '@mui/icons-material/Star';
import StarOutlineIcon from '@mui/icons-material/StarOutline';
import CloseIcon from '@mui/icons-material/Close';
// style
import styles from '../../styles/WishlistCard.module.css';
// data
import pImg from '../../assets/images/pImage.png';
import vegLarge from '../../assets/images/vegLarge.png';
import nonvegLarge from '../../assets/images/nonvegLarge.png';

const WishlistCard = () => {
  const product = {
    image: pImg,
    productName: 'Apple Chicken Salad',
    productType: 'veg',
    rating: 4,
    totalRatings: 43,
    mrp: 500.0,
    price: 249.0,
  };
  return (
    <div className={styles.wishlistCard}>
        <div className={styles.remove}>
            <CloseIcon fontSize='large' />
        </div>
      <div className={styles.wishlistCardImage}>
        <Image src={product.image} alt="Product" />
      </div>
      <div className={styles.productDetails}>
        <div className={styles.productName}>{product.productName}</div>
        <div className="flex py-4 items-center">
          <div className={styles.productType}>
            {product.productType == 'veg' ? (
              <Image src={vegLarge} alt="Veg" />
            ) : (
              <Image src={nonvegLarge} alt="Nonveg" />
            )}
          </div>
          <div className={styles.productRatings}>
            {[...Array(5)].map((x, idx) =>
              idx < product.rating ? (
                <StarIcon key={Math.random() * 999999} fontSize={'large'} />
              ) : (
                <StarOutlineIcon
                  key={Math.random() * 999999}
                  fontSize={'large'}
                />
              )
            )}
            {`(${product.totalRatings})`}
          </div>
        </div>
        <div className="flex items-center">
          <div className={styles.productMrp}>
            <del>Rs. {product.mrp}.00</del>
          </div>
          <div className={styles.productPrice}>Rs. {product.price}.00</div>
        </div>
      </div>
    </div>
  );
};

export default WishlistCard;
