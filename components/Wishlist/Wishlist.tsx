import React from 'react';
import { WishlistCard } from '..';
// styles
import styles from '../../styles/Wishlist.module.css';

const Wishlist = () => {
  return (
    <div className={`w-full ${styles.wishlist}`}>
      <div className={styles.wishlistTitle}>My Wishlists</div>
      <div className="mt-10 grid grid-cols-2 gap-8">
        <WishlistCard />
        <WishlistCard />
        <WishlistCard />
        <WishlistCard />
        <WishlistCard />
        <WishlistCard />
      </div>
    </div>
  );
};

export default Wishlist;
