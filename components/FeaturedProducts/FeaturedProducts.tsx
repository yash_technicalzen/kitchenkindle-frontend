import React from 'react';
import { useRouter } from 'next/router';

// styles
import Classes from '../../styles/FeaturedProducts.module.css';
import { ProductCard } from '..';

// data
import Button from '../Button/Button';

const FeaturedProducts = () => {
  const router = useRouter();
  // states
  return (
    <div>
      <div className="flex justify-between items-center mb-20">
        <h1 className={Classes.productGridTitle}>Featured Products</h1>
        <Button
          text="View All"
          onClick={() => router.push('/products?category=Featured-products')}
        />
      </div>
      <div className={Classes.productGridContainer}>
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard className={`${Classes.w2} ${Classes.h2}`} size="large" />
        <ProductCard className={`${Classes.w2} ${Classes.h2}`} size="large" />
      </div>
    </div>
  );
};

export default FeaturedProducts;
