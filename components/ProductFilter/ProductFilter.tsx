import { useRouter } from 'next/router';
import React from 'react';
// style
import styles from '../../styles/ProductFilter.module.css';
// material ui components
import { Checkbox } from '@mui/material';

const ProductFilter = () => {
  const router = useRouter();
  // state
  let categories = router.query.category || [];
  let productType = router.query.type || [];
  categories = categories.length > 0 ? categories?.toString().split(',') : [];
  productType =
    productType.length > 0 ? productType?.toString().split(',') : [];
  const appliedFilters = [...categories, ...productType];
  const filters = [
    {
      categoryName: 'Categories',
      categoryList: [
        {
          categoryName: 'Deals of the Day',
          isSelected: appliedFilters.includes('Deals-of-the-Day'),
        },
        {
          categoryName: 'Featured products',
          isSelected: appliedFilters.includes('Featured-products'),
        },
        {
          categoryName: 'Care',
          isSelected: appliedFilters.includes('Care'),
        },
        {
          categoryName: 'Craving',
          isSelected: appliedFilters.includes('Craving'),
        },
        {
          categoryName: 'Comfort',
          isSelected: appliedFilters.includes('Comfort'),
        },
      ],
    },
    {
      categoryName: 'Product Type',
      categoryList: [
        {
          categoryName: 'Veg',
          isSelected: appliedFilters.includes('Veg'),
        },
        {
          categoryName: 'Non-Veg',
          isSelected: appliedFilters.includes('Non-Veg'),
        },
      ],
    },
  ];

  // function
  const handleFilterChange = async (
    category: string,
    categoryItem: string,
    isSelected: boolean
  ) => {
    let categories = router.query.category || [];
    let productType = router.query.type || [];
    categories = categories.length > 0 ? categories?.toString().split(',') : [];
    productType =
      productType.length > 0 ? productType?.toString().split(',') : [];

    if (category.toLowerCase() === 'Categories'.toLowerCase()) {
      if (isSelected) {
        categories = categories?.filter(
          (cat) => cat !== categoryItem.trim().split(' ').join('-')
        );
      } else {
        categories?.push(categoryItem.trim().split(' ').join('-'));
      }
    } else if (category.toLowerCase() === 'Product Type'.toLowerCase()) {
      if (isSelected) {
        productType = productType?.filter(
          (cat) => cat !== categoryItem.trim().split(' ').join('-')
        );
      } else {
        productType?.push(categoryItem.trim().split(' ').join('-'));
      }
    }
    await router.push({
      pathname: router.pathname,
      query: {
        category: categories,
        type: productType,
      },
    });
  };
  
  return (
    <div className={styles.productFilter}>
      {appliedFilters.length > 0 && (
        <div className={styles.appliedFilters}>
          <div className="flex justify-between items-center">
            <div className={styles.filterCategoryHeading}>Filters</div>
            <div
              className={styles.filterCategoryClear}
              onClick={(e) => router.push('/products')}
            >
              Clear All
            </div>
          </div>
          <div className={styles.appliedFiltersBox}>
            {appliedFilters.map((filter) => (
              <div
                className={styles.appliedFiltersItem}
                key={Math.random() * 9999999}
              >
                {filter.split('-').join(' ')}
              </div>
            ))}
          </div>
        </div>
      )}
      {filters.map((filter) => (
        <div className="w-100" key={Math.random() * 999999}>
          <div className={styles.filterCategoryHeading}>
            {filter.categoryName}
          </div>
          <div className={styles.filterCategoryListBox}>
            {filter.categoryList.map((filterCategory) => (
              <div
                className={styles.filterCategoryListItem}
                key={Math.random() * 99999999}
              >
                <Checkbox
                  sx={{ '& .MuiSvgIcon-root': { fontSize: 28 } }}
                  className="mr-6"
                  checked={filterCategory.isSelected}
                  onChange={(e) =>
                    handleFilterChange(
                      filter.categoryName,
                      filterCategory.categoryName,
                      filterCategory.isSelected
                    )
                  }
                  inputProps={{ 'aria-label': 'controlled' }}
                />
                {filterCategory.categoryName}
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default ProductFilter;
