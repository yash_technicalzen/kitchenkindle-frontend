import Head from 'next/head';
import React, { FunctionComponent } from 'react';
import { Header, Footer } from '..';

// types
type Props = {
  pageTitle: string;
  header?: boolean;
  footer?: boolean;
  children?: React.ReactChildren | React.ReactChild;
};

const Layout: FunctionComponent<Props> = ({
  pageTitle,
  children,
  header = true,
  footer = true,
}) => {
  return (
    <>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      {header && <Header />}
      <div className="container m-auto">{children}</div>
      {footer && <Footer />}
    </>
  );
};

export default Layout;
