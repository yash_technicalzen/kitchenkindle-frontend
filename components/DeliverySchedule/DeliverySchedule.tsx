import React, {
  Dispatch,
  FunctionComponent,
  SetStateAction,
  useState,
} from 'react';
// style
import styles from '../../styles/DeliverySchedule.module.css';
// material ui components
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  TextField,
} from '@mui/material';
import { style } from '@mui/system';
// types
type DeliveryDayProps = {
  day: string;
  date: string;
  selectedDay: string;
  setSelectedDay: Dispatch<SetStateAction<string>>;
};

const DeliveryDay: FunctionComponent<DeliveryDayProps> = ({
  day,
  date,
  selectedDay,
  setSelectedDay,
}) => {
  return (
    <div
      className={`${selectedDay === date && styles.selected} ${
        styles.deliveryDayBox
      }`}
      onClick={() => setSelectedDay(date)}
    >
      <div className="text-3xl mb-4 font-bold">{day}</div>
      <div className="text-2xl">{date}</div>
    </div>
  );
};

const DeliverySchedule = () => {
  // state
  const [selectedDay, setSelectedDay] = useState('2 Mar');
  return (
    <div className=" my-12">
      <div className="flex">
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Sun"
          date="2 Mar"
        />
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Mon"
          date="3 Mar"
        />
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Tue"
          date="4 Mar"
        />
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Wed"
          date="5 Mar"
        />
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Thu"
          date="6 Mar"
        />
        <DeliveryDay
          selectedDay={selectedDay}
          setSelectedDay={setSelectedDay}
          day="Fri"
          date="7 Mar"
        />
      </div>
      <div className=" ml-10 flex mt-20">
        <FormControl>
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
            defaultValue={'lunch'}
          >
            <FormControlLabel
              value="lunch"
              control={
                <Radio
                  sx={{
                    '& .MuiSvgIcon-root': {
                      fontSize: 32,
                      color: 'var(--orange)',
                    },
                  }}
                />
              }
              label="Lunch Slot (10:30 AM - 12:30 PM)"
              sx={{
                '& .MuiFormControlLabel-label': {
                  fontSize: '1.7rem',
                },
              }}
            />
            <FormControlLabel
              value="dinner"
              control={
                <Radio
                  sx={{
                    '& .MuiSvgIcon-root': {
                      fontSize: 32,
                      color: 'var(--orange)',
                    },
                  }}
                />
              }
              label="Dinner Slot (05:30 PM - 07:30 PM)"
              sx={{
                '& .MuiFormControlLabel-label': {
                  fontSize: '1.7rem',
                },
              }}
            />
          </RadioGroup>
        </FormControl>
      </div>
      <div className="mt-32 ml-8">
        <div>
          <TextField
            id="shipping_instructions"
            label="Any Special Instructions"
            variant="outlined"
            size="medium"
            margin="normal"
            multiline
            rows={7}
            fullWidth
            placeholder={`Eg. 1 Avcados should be ripe. I want to eat them right away. \nEg. 2 I have to go to work, please try to deliver before 10:00 AM`}
          />
        </div>
      </div>
    </div>
  );
};

export default DeliverySchedule;
