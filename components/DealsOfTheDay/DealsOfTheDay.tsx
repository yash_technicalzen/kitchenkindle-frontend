import React from 'react';

// styles
import Classes from '../../styles/DealsOfTheDay.module.css';
import { ProductCard } from '..';

// data
import pImg from '../../assets/images/pImage.png';

const DealsOfTheDay = () => {
  // states
  return (
    <div>
      <h1 className={Classes.productGridTitle}>Deals of the Day</h1>
      <div className={Classes.productGridContainer}>
        <ProductCard className={`${Classes.w2} ${Classes.h2}`} size="large" />
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
        <ProductCard size="small" />
      </div>
    </div>
  );
};

export default DealsOfTheDay;
