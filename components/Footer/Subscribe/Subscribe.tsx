import Image from 'next/image';
import React, { FunctionComponent } from 'react';

// data
import email from '../../../assets/images/email.png';
import send from '../../../assets/images/send.png';

// types
type Props = {
  className: string;
};

const Subscribe: FunctionComponent<Props> = ({ className }) => {
  return (
    <form>
      <div className={className}>
        <Image src={email} alt="Email" />
        <input
          type="text"
          placeholder="Enter your email.. "
          className="w-full outline-none pr-5 bg-inherit text-inherit py-1 pl-5"
        />
        <button type="submit" className="flex ml">
          <Image src={send} alt="Send" className="cursor-pointer" />
        </button>
      </div>
    </form>
  );
};

export default Subscribe;
