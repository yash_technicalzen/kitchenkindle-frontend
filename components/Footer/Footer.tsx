import React from 'react';
import Image from 'next/image';

// styles
import Classes from '../../styles/Footer.module.css';

// data
import logo from '../../assets/images/logo.png';
import fb_icon from '../../assets/images/fb_icon.png';
import insta_icon from '../../assets/images/insta_icon.png';
import youtube_icon from '../../assets/images/youtube_icon.png';
import airtel from '../../assets/images/airtel.png';
import amex from '../../assets/images/amex.png';
import freecharge from '../../assets/images/freecharge.png';
import mastercard from '../../assets/images/mastercard.png';
import paytm from '../../assets/images/paytm.png';

// components
import Subscribe from './Subscribe/Subscribe';

const Footer = () => {
  return (
    <footer className={Classes.footer}>
      <div className="container">
        <div className={Classes.footerWrapper}>
          <div className={Classes.topFooter}>
            <div className={Classes.topFooterCol}>
              <div className={Classes.footerLogo}>
                <Image src={logo} alt="Footer Logo" />
              </div>
              <div className={Classes.footerAbout}>
                We offers high-quality foods and the best delivery service, and
                the food market you can blindly trust
              </div>
              <div className={Classes.footerSocialIconsBox}>
                <div className={Classes.footerSocialIcon}>
                  <Image src={fb_icon} alt="Facebook" />
                </div>
                <div className={Classes.footerSocialIcon}>
                  <Image src={insta_icon} alt="Instagram" />
                </div>
                <div className={Classes.footerSocialIcon}>
                  <Image src={youtube_icon} alt="Youtube" />
                </div>
              </div>
            </div>
            <div className={Classes.topFooterCol}>
              <h3 className={Classes.topFooterColHeading}>Our Information</h3>
              <div className={Classes.topFooterColList}>
                <p className={Classes.topFooterColListItem}>Privacy Policy</p>
                <p className={Classes.topFooterColListItem}>Refund Policy</p>
                <p className={Classes.topFooterColListItem}>Terms of Service</p>
                <p className={Classes.topFooterColListItem}>Blogs</p>
                <p className={Classes.topFooterColListItem}>Contact us</p>
              </div>
            </div>
            <div className={Classes.topFooterCol}>
              <h3 className={Classes.topFooterColHeading}>Subscribe Now</h3>
              <div className={Classes.footerAbout}>
                Subscribe your email for newsletter and featured news based on
                your interest
              </div>
              <Subscribe className={Classes.subscribe} />
            </div>
          </div>
          <div className={Classes.line}></div>
          <div className={Classes.bottomFooter}>
            <div className={Classes.bottomFooterCopyright}>
              © Copyright 2021 <b>Kitchen Kindle.</b> All rights reserved
            </div>
            <div className={Classes.paymentMethods}>
              <div className={Classes.paymentMethodsIcon}>
                <Image src={airtel} alt="Airtel" />
              </div>
              <div className={Classes.paymentMethodsIcon}>
                <Image src={amex} alt="Amex" />
              </div>
              <div className={Classes.paymentMethodsIcon}>
                <Image src={freecharge} alt="Freecharge" />
              </div>
              <div className={Classes.paymentMethodsIcon}>
                <Image src={mastercard} alt="Master Card" />
              </div>
              <div className={Classes.paymentMethodsIcon}>
                <Image src={paytm} alt="Paytm" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
