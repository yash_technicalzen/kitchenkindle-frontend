import React, { FunctionComponent, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
// styles
import styles from '../../styles/CartDrawer.module.css';
// material ui components
import { Drawer, IconButton } from '@mui/material';
import { Button, CartProduct } from '..';
// data
import emptyCart from '../../assets/images/emptyCart.png';
import Image from 'next/image';
// hooks
import { useAppSelector } from '../../redux/hooks';
// interface
import { Product } from '../../interface/index';
// type
type Props = {
  openCart: boolean;
  handleCartOpen: () => void;
  handleCartClose: () => void;
};

type CartProducts = {
  product: Product;
  qty: number;
};

const CartDrawer: FunctionComponent<Props> = ({
  openCart,
  handleCartClose,
  handleCartOpen,
}) => {
  const router = useRouter();
  // state
  const { products: cartProducts } = useAppSelector((state) => state.cart);
  const [products, setProducts] = useState<CartProducts[]>([]);
  const [subtotal, setSubtotal] = useState(0);
  // function
  useEffect(() => {
    setProducts(cartProducts);

    function calculateSubtotal() {
      let subtotal = 0;
      cartProducts.forEach((p) => {
        subtotal += p.product.price * p.qty;
      });
      setSubtotal(subtotal);
    }

    calculateSubtotal();
  }, [cartProducts]);

  return (
    <div className={styles.cartDrawer}>
      <Drawer anchor="right" open={openCart} onClose={handleCartClose}>
        <div className={styles.cartDrawerContent}>
          <div className={styles.cartDrawerHeading}>
            Shopping Cart
            <IconButton onClick={handleCartClose} className="py-6">
              <div className={styles.close}></div>
            </IconButton>
          </div>
          {/* Cart Items */}
          <div className={styles.cartList}>
            {products.length === 0 ? (
              <div className={styles.cartEmpty}>
                <Image src={emptyCart} alt="Empty Cart" />
                <div className={styles.cartEmptyHeading}>
                  Your cart is empty
                </div>
                <div className={styles.cartEmptyInfo}>
                  Please add product to your cart list
                </div>
              </div>
            ) : (
              products.map((p) => (
                <CartProduct
                  key={Math.random() * 999999}
                  product={p.product}
                  qty={p.qty}
                />
              ))
            )}
          </div>
          {/* Cart Items */}
          <div className={styles.cartDrawerFooter}>
            <div className={styles.cartSubtotalWrapper}>
              <div className={styles.cartSubtotalHeading}>Subtotal:</div>
              <div className={styles.cartSubtotal}>Rs. {subtotal}.00</div>
            </div>
            <div className={`w-full mt-8 text-2xl ${styles.cartInfo}`}>
              Final price and discounts will be determined at the time of
              payment processing.
            </div>
            <div className={`w-full mt-8`}>
              <Button
                onClick={() => router.push('/checkout')}
                text="Proceed to Checkout"
                width="full"
                disabled={subtotal === 0}
              />
            </div>
          </div>
        </div>
      </Drawer>
    </div>
  );
};

export default CartDrawer;
