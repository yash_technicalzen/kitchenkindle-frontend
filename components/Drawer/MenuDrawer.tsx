import React, { FunctionComponent, useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
// styles
import styles from '../../styles/MenuDrawer.module.css';
// material ui components
import {
  Drawer,
  IconButton,
  MenuList,
  MenuItem,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@mui/material';
// material ui icons
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
// data
import logo from '../../assets/images/logo.png';
import fb_icon from '../../assets/images/fb_icon.png';
import insta_icon from '../../assets/images/insta_icon.png';
import youtube_icon from '../../assets/images/youtube_icon.png';
// type
type Props = {
  openMenu: boolean;
  handleMenuOpen: () => void;
  handleMenuClose: () => void;
};

const MenuDrawer: FunctionComponent<Props> = ({
  openMenu,
  handleMenuClose,
  handleMenuOpen,
}) => {
  const router = useRouter();
  return (
    <div className={styles.menuDrawer}>
      <Drawer anchor="left" open={openMenu} onClose={handleMenuClose}>
        <div className={styles.menuDrawerContent}>
          <div className={styles.menuDrawerHeading}>
            <Image
              src={logo}
              alt="Logo"
              className={styles.headerLogo}
              onClick={(e) => router.push('/')}
            />
            <IconButton onClick={handleMenuClose} className='py-6'>
              <div className={styles.close}></div>
            </IconButton>
          </div>
          {/* Menu Items */}
          <div className={styles.menuList}>
            <MenuList>
              <MenuItem className={styles.menuItem}>Home</MenuItem>
              <Accordion disableGutters className={styles.menuItem}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon fontSize="large" />}
                >
                  Categories
                </AccordionSummary>
                <AccordionDetails>
                  <MenuItem className={styles.menuSubItem}>Care</MenuItem>
                  <MenuItem className={styles.menuSubItem}>Comfort</MenuItem>
                  <MenuItem className={styles.menuSubItem}>Craving</MenuItem>
                </AccordionDetails>
              </Accordion>
              <MenuItem className={styles.menuItem}>About Us</MenuItem>
              <MenuItem className={styles.menuItem}>FAQs</MenuItem>
              <MenuItem className={styles.menuItem}>Contact Us</MenuItem>
              <MenuItem className={styles.menuItem}>How to use?</MenuItem>
            </MenuList>
          </div>
          {/* Menu Items */}
          <div className={styles.menuDrawerFooter}>
            <div className={styles.menuDrawerIcon}>
              <Image src={fb_icon} alt="Facebook" />
            </div>
            <div className={styles.menuDrawerIcon}>
              <Image src={insta_icon} alt="Instagram" />
            </div>
            <div className={styles.menuDrawerIcon}>
              <Image src={youtube_icon} alt="Youtube" />
            </div>
          </div>
        </div>
      </Drawer>
    </div>
  );
};

export default MenuDrawer;
