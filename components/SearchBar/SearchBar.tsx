import Image from 'next/image';
import React, { FunctionComponent } from 'react';

// data
import search from '../../assets/images/search.svg';

// types
type Props = {
  className: string;
  placeholder?: string
};

const SearchBar: FunctionComponent<Props> = ({ className, placeholder }) => {
  return (
    <form>
      <div className={className}>
        <input
          type="text"
          placeholder={placeholder || "What are you looking.. "}
          className="w-full outline-none pr-5 bg-inherit text-inherit py-2 pl-8 ml-4"
        />
        <button type="submit" className="flex ml">
          <Image src={search} alt="Search" className="cursor-pointer" />
        </button>
      </div>
    </form>
  );
};

export default SearchBar;
