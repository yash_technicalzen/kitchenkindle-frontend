import React, { FunctionComponent } from 'react';
// style
import styles from '../../styles/CartProduct.module.css';
// material ui components
import { IconButton } from '@mui/material';
// material ui icons
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import DeleteIcon from '@mui/icons-material/Delete';
// data
import pImg from '../../assets/images/pImage.png';
import Image from 'next/image';
// reducers
import {
  addProductToCart,
  subProductFromCart,
  delProductFromCart,
} from '../../redux/reducers/cart.reducer';
// hooks
import { useAppDispatch } from '../../redux/hooks';
// interface
import { Product } from '../../interface';
// type
type Props = {
  product: Product;
  qty: number;
};

const CartProduct: FunctionComponent<Props> = ({ product, qty }) => {
  const dispatch = useAppDispatch();
  // state

  // function
  async function addProductToCartHandler(product: Product, qty: number) {
    const res = await dispatch(addProductToCart({ product, qty }));
  }

  async function subProductFromCartHandler(product: Product, qty: number) {
    const res = await dispatch(subProductFromCart({ product, qty }));
  }

  async function delProductFromCartHandler(product: Product) {
    const res = await dispatch(delProductFromCart({ product }));
  }
  return (
    <div className={styles.cartProduct}>
      <IconButton
        className={`mr-8 ${styles.cartProductDelCta}`}
        onClick={(e) => delProductFromCartHandler(product)}
      >
        <DeleteIcon fontSize="large" />
      </IconButton>
      <div className={styles.cartProductImage}>
        <Image src={pImg} alt="Product" layout="fill" objectFit="contain" />
      </div>
      <div className={styles.cartProductDetails}>
        <div className={styles.cartProductName}>{product.name}</div>
        <div className="flex items-center mb-6">
          <div className={styles.cartProductMrp}>
            <del>Rs. {product.mrp}.00</del>
          </div>
          <div className={styles.cartProductPrice}>Rs. {product.price}.00</div>
        </div>
        <div className="flex items-center">
          <IconButton
            className={`mr-8 ${styles.cartProductCta}`}
            onClick={(e) => addProductToCartHandler(product, 1)}
          >
            <AddIcon fontSize="large" />
          </IconButton>
          <div className={styles.cartProductQty}>{qty}</div>
          <IconButton
            className={`ml-8 ${styles.cartProductCta}`}
            onClick={(e) => subProductFromCartHandler(product, 1)}
            disabled={qty === 1}
          >
            <RemoveIcon fontSize="large" />
          </IconButton>
        </div>
      </div>
      <div className={styles.cartProductSubtotal}>
        Rs. {product.price * qty}.00
      </div>
    </div>
  );
};

export default CartProduct;
