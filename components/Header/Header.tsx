import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { FunctionComponent, useState } from 'react';
// hooks
import { useAppSelector } from '../../redux/hooks';
// styles
import styles from '../../styles/Header.module.css';
// data
import logo from '../../assets/images/logo.png';
import shopping_bag from '../../assets/images/shopping_bag.svg';
import account from '../../assets/images/account.svg';
// components
import { CartDrawer, MenuDrawer, SearchBar } from '..';
const Header: FunctionComponent = () => {
  const router = useRouter();
  // state
  const { user, loggedIn } = useAppSelector((state) => state.authReducer);
  const { products } = useAppSelector((state) => state.cart);
  // state
  const [openMenu, setOpenMenu] = useState<boolean>(false);
  const [openCart, setOpenCart] = useState<boolean>(false);
  // function
  const handleMenuOpen = () => {
    setOpenMenu(true);
  };
  const handleMenuClose = () => {
    setOpenMenu(false);
  };
  const handleCartOpen = () => {
    setOpenCart(true);
  };
  const handleCartClose = () => {
    setOpenCart(false);
  };
  return (
    <header className={styles.header}>
      <div className="mx-16 w-full">
        <div className={styles.headerWrapper}>
          <div className={styles.leftHeader}>
            <div
              onClick={handleMenuOpen}
              className="cursor-pointer h-14 flex justify-center items-center"
            >
              <div className={styles.hamburger}></div>
            </div>
            <Image
              src={logo}
              alt="Logo"
              className={styles.headerLogo}
              onClick={(e) => router.push('/')}
            />
          </div>
          <div className={styles.search}>
            <SearchBar className={styles.searchBar} />
          </div>
          <div className={styles.rightHeader}>
            <div
              className={styles.account}
              onClick={(e) => {
                if (loggedIn) {
                  router.push('/dashboard/user/1');
                } else {
                  router.push('/auth/login');
                }
              }}
            >
              <Image
                src={account}
                alt="Account"
                className={styles.accountIcon}
              />
              <h4 className={styles.accountText}>
                {loggedIn ? user?.firstName : 'Login'}
              </h4>
            </div>
            <div className={styles.shoppingBag} onClick={handleCartOpen}>
              <div className="relative">
                <Image
                  src={shopping_bag}
                  alt="Shopping Bag"
                  className={styles.shoppingBagIcon}
                />
                <div className={styles.shoppingBagItems}>{products.length}</div>
              </div>
              <h4 className={styles.shoppingBagText}>Cart</h4>
            </div>
          </div>
        </div>
        <MenuDrawer
          openMenu={openMenu}
          handleMenuClose={handleMenuClose}
          handleMenuOpen={handleMenuOpen}
        />
        <CartDrawer
          openCart={openCart}
          handleCartClose={handleCartClose}
          handleCartOpen={handleCartOpen}
        />
      </div>
    </header>
  );
};

export default Header;
