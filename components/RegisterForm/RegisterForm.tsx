import React from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { registerSchema } from '../../utils/validationSchemas';

// hooks
import { useAppDispatch } from '../../redux/hooks';

// reducers
import { registerUserWithEmail } from '../../redux/reducers/auth.reducer';
import {
  errorMessage,
  successMessage,
} from '../../redux/reducers/alert.reducer';

// components
import { Button } from '../../components';

// material ui components
import { TextField } from '@mui/material';

// styles
import Classes from '../../styles/Form.module.css';

// data
import logo from '../../assets/images/logo.png';

const RegisterForm = () => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  // state

  // form
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(registerSchema) });

  // function
  const handleRegister = async (formData: any) => {
    const res: any = await dispatch(registerUserWithEmail(formData));
    if (res.payload.error) {
      dispatch(errorMessage(res.payload.error));
    } else {
      dispatch(successMessage('Registered Successfully'));
      if (router.query?.redirect) {
        router.push(router.query.redirect.toString());
      } else {
        router.push('/dashboard/user/1');
      }
    }
  };

  return (
    <div className={Classes.loginForm}>
      <div className={Classes.formLogo}>
        <Image src={logo} alt="Form Logo" />
      </div>
      <div className={Classes.formTitle}>Register for free!</div>
      <div className={Classes.formSubtitle}>
        Already Registered?{' '}
        <span
          onClick={(e) => {
            if (router.query?.redirect) {
              router.push('/auth/login?redirect=' + router.query?.redirect[0]);
            } else {
              router.push('/auth/login');
            }
          }}
        >
          Login Now
        </span>
      </div>
      <div className="mt-10 w-[80%]">
        <form className="w-full" onSubmit={handleSubmit(handleRegister)}>
          <TextField
            id="register_fname"
            label="First Name"
            variant="outlined"
            {...register('firstName')}
            size="medium"
            margin="normal"
            error={errors.firstName?.message === undefined ? false : true}
            helperText={errors.firstName?.message}
            fullWidth
          />
          <TextField
            id="register_lname"
            label="Last Name"
            variant="outlined"
            {...register('lastName')}
            size="medium"
            margin="normal"
            error={errors.lastName?.message === undefined ? false : true}
            helperText={errors.lastName?.message}
            fullWidth
          />
          <TextField
            id="register_email"
            label="Email Address"
            variant="outlined"
            {...register('email')}
            size="medium"
            margin="normal"
            error={errors.email?.message === undefined ? false : true}
            helperText={errors.email?.message}
            fullWidth
          />
          <TextField
            id="register_password"
            label="Password"
            variant="outlined"
            {...register('password')}
            size="medium"
            type={'password'}
            margin="normal"
            error={errors.password?.message === undefined ? false : true}
            helperText={errors.password?.message}
            fullWidth
          />
          <Button text="Register" width="full" type="submit" />
        </form>
      </div>
    </div>
  );
};

export default RegisterForm;
