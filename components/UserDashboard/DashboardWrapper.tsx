import React, { FunctionComponent } from 'react';
// components
import {
  AccountSettings,
  Address,
  ChangePassword,
  DashboardOptions,
  Orders,
  Wishlist,
} from '..';
// types
type Props = {
  selectedOption?: string;
};

const DashboardWrapper: FunctionComponent<Props> = ({ selectedOption }) => {
  // state

  //function
  const dashboardComponent = () => {
    switch (selectedOption) {
      case '1':
        return <AccountSettings />;
      case '2':
        return <Orders />;
      case '3':
        return <Wishlist />;
      case '4':
        return <Address />;
      case '5':
        return <ChangePassword />;
    }
  };
  return (
    <div className="w-full flex">
      <div className="w-[25%] mr-4">
        <DashboardOptions selectedOption={selectedOption} />
      </div>
      <div className="w-[75%]">{dashboardComponent()}</div>
    </div>
  );
};

export default DashboardWrapper;
