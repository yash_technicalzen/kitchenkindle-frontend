import React, { FunctionComponent } from 'react';
import Image from 'next/image';
import Link from 'next/link';
// styles
import styles from '../../styles/DashboardOptions.module.css';
// data
import dashboardAccount from '../../assets/images/dashboard-account.png';
import dashboardBag from '../../assets/images/dashboard-bag.png';
import dashboardLike from '../../assets/images/dashboard-like.png';
import dashboardLocation from '../../assets/images/dashboard-location.png';
import dashboardSettings from '../../assets/images/dashboard-settings.png';
import dashboardLogout from '../../assets/images/dashboard-logout.png';
// hooks
import { useAppDispatch } from '../../redux/hooks';
// reducers
import { logoutUser } from '../../redux/reducers/auth.reducer';
import { successMessage } from '../../redux/reducers/alert.reducer';

// types
type Props = {
  selectedOption?: string;
};

const DashboardOptions: FunctionComponent<Props> = ({ selectedOption }) => {
  const dispatch = useAppDispatch();
  // state
  const optionsList = [
    {
      id: 1,
      icon: dashboardAccount,
      title: 'Account Settings',
      addLine: true,
      route: '/dashboard/user/1',
    },
    {
      id: 2,
      icon: dashboardBag,
      title: 'Orders',
      addLine: true,
      route: '/dashboard/user/2',
    },
    {
      id: 3,
      icon: dashboardLike,
      title: 'Wishlist',
      addLine: true,
      route: '/dashboard/user/3',
    },
    {
      id: 4,
      icon: dashboardLocation,
      title: 'Address',
      addLine: true,
      route: '/dashboard/user/4',
    },
    {
      id: 5,
      icon: dashboardSettings,
      title: 'Change Password',
      addLine: true,
      route: '/dashboard/user/5',
    },
    {
      id: 6,
      icon: dashboardLogout,
      title: 'Logout',
      addLine: false,
      route: '/',
      onClick: async () => {
        await dispatch(logoutUser());
        await dispatch(successMessage('Logout Successfully'));
      },
    },
  ];

  // function
  return (
    <div className={`w-full mr-4 ${styles.optionsContainer}`}>
      {optionsList.map((item) => (
        <div key={Math.random() * 99999999} onClick={item.onClick}>
          <Link href={item.route} passHref>
            <div
              className={`${styles.optionItem} ${
                item.id === 1 && styles.topRounded
              } ${item.id.toString() === selectedOption && styles.selected}`}
            >
              <div className={`${styles.optionItemIcon}`}>
                <Image src={item.icon} alt={item.title} />
              </div>
              <div className={`${styles.optionItemTitle}`}>{item.title}</div>
            </div>
          </Link>
          {item.addLine && <div className={styles.optionItemLine}></div>}
        </div>
      ))}
    </div>
  );
};

export default DashboardOptions;
