import React, { useEffect, useState } from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { updateProfileSchema } from '../../utils/validationSchemas';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
// hooks
import { useAppSelector } from '../../redux/hooks';
// material ui components
import { TextField } from '@mui/material';
// styles
import styles from '../../styles/AccountSettings.module.css';
// components
import { Button } from '..';
// type
interface User {
  _id: string;
  userId: string;
  userEmail?: string;
  mobile?: number | string;
  firstName: string;
  lastName: string;
  emailVerified: boolean;
  mobileVerified: boolean;
  username: string;
  registeredAt: string;
  sessionId: string;
}

const AccountSettings = () => {
  const { user } = useAppSelector((state) => state.authReducer);
  // state
  const [firstName, setFirstName] = useState(user?.firstName);
  const [lastName, setLastName] = useState(user?.lastName);
  const [email, setEmail] = useState(user?.userEmail);
  const [mobile, setMobile] = useState(user?.mobile);

  // form
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(updateProfileSchema) });

  // function
  useEffect(() => {
    if (user) {
      setFirstName(user?.firstName);
      setLastName(user?.lastName);
      setEmail(user?.userEmail);
      setMobile(user?.mobile);
    }
  }, [user]);

  const handleProfileChange = async (formData: any) => {
    console.log(formData);
  };
  return (
    <div className={`w-full ${styles.accountSettings}`}>
      <div className={styles.accountSettingsTitle}>Account Settings</div>
      <form className="w-full mt-10" onSubmit={handleSubmit(handleProfileChange)}>
        <div className="flex">
          <div className="w-[50%] mr-2">
            <TextField
              id="profile_fname"
              label="First Name"
              variant="outlined"
              {...register('firstName')}
              size="medium"
              margin="normal"
              defaultValue={firstName}
              error={errors.firstName?.message === undefined ? false : true}
              helperText={errors.firstName?.message}
              fullWidth
            />
          </div>
          <div className="w-[50%] ml-2">
            <TextField
              id="profile_lname"
              label="Last Name"
              variant="outlined"
              {...register('lastName')}
              size="medium"
              margin="normal"
              defaultValue={lastName}
              error={errors.lastName?.message === undefined ? false : true}
              helperText={errors.lastName?.message}
              fullWidth
            />
          </div>
        </div>
        <div className="flex">
          <div className="w-[50%] mr-2">
            <TextField
              id="profile_email"
              label="Email Address"
              variant="outlined"
              {...register('email')}
              size="medium"
              margin="normal"
              defaultValue={email}
              error={errors.email?.message === undefined ? false : true}
              helperText={errors.email?.message}
              fullWidth
            />
          </div>
          <div className="w-[50%] ml-2">
            <TextField
              id="profile_mobile"
              label="Phone Number"
              variant="outlined"
              {...register('mobile')}
              size="medium"
              type={'number'}
              margin="normal"
              defaultValue={mobile}
              error={errors.mobile?.message === undefined ? false : true}
              helperText={errors.mobile?.message}
              fullWidth
            />
          </div>
        </div>

        <div className="mt-5">
          <Button text="Save Changes" type="submit" />
        </div>
      </form>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (
  ctx: GetServerSidePropsContext
) => {
  return {
    props: {},
  };
};

export default AccountSettings;
