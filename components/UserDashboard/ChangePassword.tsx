import React, { useEffect, useState } from 'react';
import { updatePasswordSchema } from '../../utils/validationSchemas';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
// material ui components
import { TextField } from '@mui/material';
// styles
import styles from '../../styles/ChangePassword.module.css';
// components
import { Button } from '..';
const ChangePassword = () => {
  // form
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(updatePasswordSchema) });

  // function
  const handleProfileChange = async (formData: any) => {
    console.log(formData);
  };
  return (
    <div className={`w-full ${styles.changePassword}`}>
      <div className={styles.changePasswordTitle}>Change Password</div>
      <form
        className="w-full mt-10"
        onSubmit={handleSubmit(handleProfileChange)}
      >
        <div className="w-[100%] mr-2">
          <TextField
            id="profile_op"
            label="Old Password"
            variant="outlined"
            {...register('oldPassword')}
            size="medium"
            margin="normal"
            type={'password'}
            error={errors.oldPassword?.message === undefined ? false : true}
            helperText={errors.oldPassword?.message}
            fullWidth
          />
        </div>
        <div className="w-[100%] ml-2">
          <TextField
            id="profile_np"
            label="New Password"
            variant="outlined"
            {...register('newPassword')}
            size="medium"
            margin="normal"
            type={'password'}
            error={errors.newPassword?.message === undefined ? false : true}
            helperText={errors.newPassword?.message}
            fullWidth
          />
        </div>
        <div className="w-[100%] mr-2">
          <TextField
            id="profile_cnp"
            label="Confirm New Password"
            variant="outlined"
            {...register('confirmNewPassword')}
            size="medium"
            margin="normal"
            type={'password'}
            error={
              errors.confirmNewPassword?.message === undefined ? false : true
            }
            helperText={errors.confirmNewPassword?.message}
            fullWidth
          />
        </div>

        <div className="mt-5">
          <Button text="Save Changes" type="submit" />
        </div>
      </form>
    </div>
  );
};

export default ChangePassword;
