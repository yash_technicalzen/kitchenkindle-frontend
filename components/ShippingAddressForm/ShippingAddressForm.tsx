import React from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
// style
import styles from '../../styles/ShippingAddressForm.module.css';
// hooks
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
// material ui components
import {
  TextField,
  FormControlLabel,
  RadioGroup,
  Radio,
  Checkbox,
} from '@mui/material';
import { Button } from '..';
// components

const ShippingAddressForm = () => {
  const dispatch = useAppDispatch();
  // state
  const { loggedIn, user } = useAppSelector((state) => state.authReducer);
  return (
    <div className="ml-14 my-12">
      <div className="text-3xl">
        Already Registered?{' '}
        <span
          className="cursor-pointer ml-2"
          style={{ color: 'var(--orange)' }}
        >
          Login Now
        </span>
      </div>
      <div className="mt-6">
        <TextField
          id="shipping_email_phone"
          label="Email Address / Phone Number"
          variant="outlined"
          size="medium"
          margin="normal"
          fullWidth
        />
      </div>
      <div className={styles.line}></div>
      <form className="grid grid-cols-2 gap-6">
        <div>
          <TextField
            id="shipping_city"
            label="City"
            variant="outlined"
            size="medium"
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="shipping_pincode"
            label="Pincode"
            variant="outlined"
            size="medium"
            margin="normal"
            type={'number'}
            fullWidth
          />
        </div>
        <div className="col-span-2">
          <TextField
            id="shipping_area"
            label="Area"
            variant="outlined"
            size="medium"
            margin="normal"
            fullWidth
          />
        </div>
        <div className="col-span-2">
          <TextField
            id="shipping_address"
            label="Address Details"
            variant="outlined"
            size="medium"
            margin="normal"
            rows={'5'}
            multiline
            placeholder="Please provide details Floor, House Number, Street Locality"
            fullWidth
          />
        </div>
        <div className="col-span-2">
          <TextField
            id="shipping_landmark"
            label="Landmark"
            variant="outlined"
            size="medium"
            margin="normal"
            placeholder="(Optional)"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="shipping_name"
            label="Name"
            variant="outlined"
            size="medium"
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="shipping_email"
            label="Email Address"
            variant="outlined"
            size="medium"
            margin="normal"
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="shipping_mobile"
            label="Phone Number"
            variant="outlined"
            size="medium"
            margin="normal"
            type={'number'}
            fullWidth
          />
        </div>
        <div>
          <TextField
            id="shipping_alternatemobile"
            label="Alternate Phone Number"
            variant="outlined"
            size="medium"
            margin="normal"
            type={'number'}
            fullWidth
          />
        </div>
        <div className="col-span-2 mb-4">
          <div className="text-3xl my-10">Address Type</div>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue="home"
            name="radio-buttons-group"
          >
            <div className="w-100 justify-between items-center flex">
              <FormControlLabel
                value="home"
                control={
                  <Radio
                    sx={{
                      '& .MuiSvgIcon-root': {
                        fontSize: 32,
                        color: 'var(--orange)',
                      },
                    }}
                  />
                }
                label="Home"
                sx={{
                  '& .MuiFormControlLabel-label': {
                    fontSize: '1.7rem',
                  },
                }}
              />
              <FormControlLabel
                value="office"
                control={
                  <Radio
                    sx={{
                      '& .MuiSvgIcon-root': {
                        fontSize: 32,
                        color: 'var(--orange)',
                      },
                    }}
                  />
                }
                label="Office"
                sx={{
                  '& .MuiFormControlLabel-label': {
                    fontSize: '1.7rem',
                  },
                }}
              />
              <FormControlLabel
                value="other"
                control={
                  <Radio
                    sx={{
                      '& .MuiSvgIcon-root': {
                        fontSize: 32,
                        color: 'var(--orange)',
                      },
                    }}
                  />
                }
                label="Other"
                sx={{
                  '& .MuiFormControlLabel-label': {
                    fontSize: '1.7rem',
                  },
                }}
              />
            </div>
          </RadioGroup>
        </div>
        <div className="mt-6">
          <FormControlLabel
            control={
              <Checkbox
                sx={{
                  '& .MuiSvgIcon-root': {
                    fontSize: 28,
                    color: 'var(--orange)',
                  },
                }}
              />
            }
            label="Save for Later"
            sx={{
              '& .MuiFormControlLabel-label': {
                fontSize: '1.7rem',
              },
            }}
          />
        </div>
        <div className="flex justify-end">
          <Button text="Next Step" />
        </div>
      </form>
    </div>
  );
};

export default ShippingAddressForm;
