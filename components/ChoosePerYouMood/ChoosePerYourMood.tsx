import React from 'react';
import Image from 'next/image';

// styles
import Classes from '../../styles/ChoosePerYourMood.module.css';

// data
import care from '../../assets/images/care.png';
import comfort from '../../assets/images/comfort.png';
import craving from '../../assets/images/craving.png';

const ChoosePerYourMood = () => {
  // states
  return (
    <div>
      <h1 className={Classes.productGridTitle}>Choose as per your mood</h1>
      <div className={Classes.productGridContainer}>
        <div>
          <Image src={care} alt="Care Mood" />
        </div>
        <div>
          <Image src={comfort} alt="Comfort Mood" />
        </div>
        <div>
          <Image src={craving} alt="Craving Mood" />
        </div>
      </div>
    </div>
  );
};

export default ChoosePerYourMood;
